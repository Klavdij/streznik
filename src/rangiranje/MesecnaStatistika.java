package rangiranje;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import entitete.Podatek;
import orodja.Baza;
import orodja.Klasifikator;
import orodja.Operater;
import orodja.nastavitve.MesecnaStatistikaNastavitve;
import orodja.nastavitve.Nastavitve;
import rangiranje.rezultati.Rezultat;

public class MesecnaStatistika {
	
	private ObjectId idUporabnika;
	private int operaterUporanika;
	
	private Baza baza;
	
	private MesecnaStatistikaNastavitve nastavitveMesecneStatistike;

	private int leto;
	private int mesec;
	
	private Podatek[] podatki;
	
	private int steviloSmsMms=0;
	private int kolicinaNet=0;
	private int trajanjeKliciDomace=0;
	private int trajanjeKliciTuje=0;
	private int trajanjeKliciStac=0;
	
	private Date meja;
	private int stDni;
	
	private Klasifikator klasSmsMms;
	private Klasifikator klasKliciDomace;
	private Klasifikator klasKliciTuje;
	private Klasifikator klasKliciStac;
	private Klasifikator klasNet;
	
	private Rezultat rezultat;
	
	public MesecnaStatistika(String idUporabnika) {
		
		this.idUporabnika = new ObjectId(idUporabnika);
		
		baza=new Baza();
		
		Nastavitve nastavitve=new Nastavitve();
		nastavitveMesecneStatistike=nastavitve.vrniNastavitveMesecneStatistike();
		
		pridobiMesecLetoObdelave();
		
		podatki=naloziPodatke();
		
		nastaviMejo();
		stDni=nastavitveMesecneStatistike.dniTestneMnozice;
		
		operaterUporanika=pridobiOperaterjaUporabnika();
		
		klasSmsMms=new Klasifikator();
		klasKliciDomace=new Klasifikator();
		klasKliciTuje=new Klasifikator();
		klasKliciStac=new Klasifikator();
		klasNet=new Klasifikator();
		
		rezultat=new Rezultat();
	}
	
	/**
	 * Nastavi mesec in leto obdelave podatkov 
	 * @param leto obdelave
	 * @param mesec od 0 do 11
	 */
	public void nastaviMesecLetoObdelave(int leto, int mesec){
		this.leto=leto;
		this.mesec=mesec;
		
		nastaviMejo();
	}
	
	/**
	 * Nastavi datum do katerega se gradi ucna mnozica.
	 */
	private void nastaviMejo(){
		GregorianCalendar m=new GregorianCalendar(leto, mesec, nastavitveMesecneStatistike.dniTestneMnozice+1);
		meja=m.getTime();
	}
	
	/**
	 * Izberemo prej�nji mesec
	 */
	private void pridobiMesecLetoObdelave(){
		GregorianCalendar danes=new GregorianCalendar();
		danes.add(GregorianCalendar.MONTH, -1);
		
		leto=danes.get(GregorianCalendar.YEAR);
		mesec=danes.get(GregorianCalendar.MONTH);
	}
	
	/**
	 * Iz baze prebere operaterja uporabnika
	 * @return operaterja
	 */
	private int pridobiOperaterjaUporabnika(){
		BasicDBObject storitev=baza.dobiStoritevUporabnika(idUporabnika);
		return Operater.vrniOperaterja(storitev.getString("operater"));
	}
	
	private Podatek[] naloziPodatke(){
		
		Podatek [] p=null;
		
		BasicDBList pList;
		BasicDBObject objekt;
		
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUporabnika);
		
		GregorianCalendar from=new GregorianCalendar(leto,mesec,1,0,0,0);
		int dni=from.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		GregorianCalendar to=new GregorianCalendar(leto,mesec,dni,0,0,0);
		
		pogoji.append("datum", new BasicDBObject("$gte", from.getTime()).append("$lte", to.getTime()));
		
		BasicDBObject sortiranje=new BasicDBObject("datum", 1);
		
		pList=baza.dobiPodatke(Podatek.ZBIRKA, pogoji, sortiranje);
		
		if(pList!=null && pList.size()>0){
			
			p=new Podatek[pList.size()];
			
			for (int i = 0; i < pList.size(); i++) {
				objekt=(BasicDBObject) pList.get(i);
				
				p[i]=new Podatek(objekt);
			}
		}
		
		return p;
	}
	
	public void obdelava(){
		
		if(podatki!=null){
			int [] simuliraniPodatki;
			int [] dejanskiPodatki;
			
			ustvariKlasifikatorje();
			
			simuliraniPodatki=simulirajPodatke();
			
			dejanskiPodatki=prestejPodatke();
			
			primerjavaPodatkov(simuliraniPodatki,dejanskiPodatki);
		}
		
	}
	
	/**
	 * Klasificira podatke glede na ucno in testno mnozico.
	 */
	private void ustvariKlasifikatorje(){
	
		izracunajPovprecja();
		
		izracunajPovprecnoOdstopanje(klasSmsMms,'m');
		izracunajPovprecnoOdstopanje(klasNet,'n');
		izracunajPovprecnoOdstopanje(klasKliciDomace,'d');
		izracunajPovprecnoOdstopanje(klasKliciTuje,'t');
		izracunajPovprecnoOdstopanje(klasKliciStac,'s');
		
		izracunajProcente(klasSmsMms, 'm');
		izracunajProcente(klasNet,'n');
		izracunajProcente(klasKliciDomace,'d');
		izracunajProcente(klasKliciTuje,'t');
		izracunajProcente(klasKliciStac,'s');
				
	}
	
	private void izracunajPovprecja(){
		int i=0;
		
		int stevilka;
		int operaterSt;
		
		do{
			
			switch (podatki[i].getTip()) {
				case 's':
					if(podatki[i].getOdhodniDohodni()==1){
						steviloSmsMms++;
					}
					break;
				case 'm':
					if(podatki[i].getOdhodniDohodni()==1){
						steviloSmsMms++;
					}
					break;
				case 'n':
					kolicinaNet+=podatki[i].getKolicina();
					break;
				case 'k':
					if(podatki[i].getOdhodniDohodni()==1){
						
						stevilka=podatki[i].getStevilka();
						operaterSt=Operater.vrniOperaterja(stevilka);
						
						if(operaterSt==Operater.STACIONARNA){
							trajanjeKliciStac+=podatki[i].getTrajanje();
						}
						else if(operaterSt==operaterUporanika){
							trajanjeKliciDomace+=podatki[i].getTrajanje();
						}
						else{
							trajanjeKliciTuje+=podatki[i].getTrajanje();
						}
					}
					break;
			}				
			
			i++;
		}while(podatki[i].getDatum().getTime()<meja.getTime());
		
		klasSmsMms.setPovprecje(steviloSmsMms/stDni);
		klasKliciDomace.setPovprecje(trajanjeKliciDomace/stDni);
		klasKliciTuje.setPovprecje(trajanjeKliciTuje/stDni);
		klasKliciStac.setPovprecje(trajanjeKliciStac/stDni);
		klasNet.setPovprecje(kolicinaNet/stDni);
	}
	
	private void izracunajPovprecnoOdstopanje(Klasifikator k, char tip){
		
		int i=0;
		int dan=0;
		
		int stevilka;
		int operaterSt;
		
		int min=Integer.MAX_VALUE;
		int max=Integer.MIN_VALUE;
		
		int razlika=0;
		int povprecnoOdstopanjeMinus=0;
		int povprecnoOdstopanjePlus=0;
		
		ArrayList<Integer> odstopanjeMinus=new ArrayList<Integer>();
		ArrayList<Integer> odstopanjePlus=new ArrayList<Integer>();
		
		int enot=0;
		
		GregorianCalendar z=new GregorianCalendar(leto, mesec, 2);
		Date dnevnaMeja=z.getTime();
		Date prejsniDan=z.getTime();
		prejsniDan.setTime(dnevnaMeja.getTime()-(60*60*24*1000));
		
		
		do{
			if(podatki[i].getDatum().getTime()<dnevnaMeja.getTime() && podatki[i].getDatum().getTime()>prejsniDan.getTime()){
				
				switch (tip) {
					case 'm':
						if(podatki[i].getTip()=='s' || podatki[i].getTip()=='m'){
							if(podatki[i].getOdhodniDohodni()==1){
								enot++;
							}
						}
						break;
					case 'n':
						if(podatki[i].getTip()=='n'){
							enot+=podatki[i].getKolicina();
						}
						break;
					case 's':
						if(podatki[i].getTip()=='k' && podatki[i].getOdhodniDohodni()==1){
							stevilka=podatki[i].getStevilka();
							operaterSt=Operater.vrniOperaterja(stevilka);
							
							if(operaterSt==Operater.STACIONARNA){
								enot+=podatki[i].getTrajanje();
							}
						}
						
						break;
					case 'd':
						if(podatki[i].getTip()=='k' && podatki[i].getOdhodniDohodni()==1){
							stevilka=podatki[i].getStevilka();
							operaterSt=Operater.vrniOperaterja(stevilka);
							
							if(operaterSt==operaterUporanika){
								enot+=podatki[i].getTrajanje();
							}
						}
						
						break;
					case 't':
						if(podatki[i].getTip()=='k' && podatki[i].getOdhodniDohodni()==1){
							stevilka=podatki[i].getStevilka();
							operaterSt=Operater.vrniOperaterja(stevilka);
							
							if(operaterSt!=operaterUporanika && operaterSt!=Operater.STACIONARNA){
								enot+=podatki[i].getTrajanje();
							}
						}
						
						break;
				}			
				
				if(i<podatki.length){
					i++;
				}
			}
			else{
				
				if(enot<min){
					min=enot;
				}
				
				if(enot>max){
					max=enot;
				}
				
				razlika=k.getPovprecje()-enot;
				
				if(razlika<0){
					odstopanjePlus.add(Math.abs(razlika));
				}
				else{
					odstopanjeMinus.add(razlika);
				}
				
				dan++;
				prejsniDan.setTime(dnevnaMeja.getTime());
				dnevnaMeja.setTime(dnevnaMeja.getTime()+(60*60*24*1000));
				
				razlika=0;
				
				enot=0;
				
			}
			
		}while(dan<stDni);
		
		for (int j = 0; j < odstopanjeMinus.size(); j++) {
			povprecnoOdstopanjeMinus+=odstopanjeMinus.get(j);
		}
		
		for (int j = 0; j < odstopanjePlus.size(); j++) {
			povprecnoOdstopanjePlus+=odstopanjePlus.get(j);
		}
		
		if(odstopanjeMinus.size()>0){
			povprecnoOdstopanjeMinus/=odstopanjeMinus.size();
		}
		if(odstopanjePlus.size()>0){
			povprecnoOdstopanjePlus/=odstopanjePlus.size();
		}
		
		k.setMin(min);
		k.setMax(max);
		k.setPovprOdstopanjeMinus(povprecnoOdstopanjeMinus);
		k.setPovprOdstopanjePlus(povprecnoOdstopanjePlus);
	}
	
	private void izracunajProcente(Klasifikator k, char tip){
		
		int i=0;
		int dan=0;
		
		int stevilka;
		int operaterSt;
		
		int enota=0;
		
		GregorianCalendar z=new GregorianCalendar(leto, mesec, 2);
		Date dnevnaMeja=z.getTime();
		Date prejsniDan=z.getTime();
		prejsniDan.setTime(dnevnaMeja.getTime()-(60*60*24*1000));
		
		
		do{
			if(podatki[i].getDatum().getTime()<dnevnaMeja.getTime() && podatki[i].getDatum().getTime()>prejsniDan.getTime()){
				
				switch (tip) {
					case 'm':
						if(podatki[i].getTip()=='s' || podatki[i].getTip()=='m'){
							if(podatki[i].getOdhodniDohodni()==1){
								enota++;
							}
						}
						break;
					case 'n':
						if(podatki[i].getTip()=='n'){
							enota+=podatki[i].getKolicina();
						}
						break;
					case 's':
						if(podatki[i].getTip()=='k' && podatki[i].getOdhodniDohodni()==1){
							stevilka=podatki[i].getStevilka();
							operaterSt=Operater.vrniOperaterja(stevilka);
							
							if(operaterSt==Operater.STACIONARNA){
								enota+=podatki[i].getTrajanje();
							}
						}
						
						break;
					case 'd':
						if(podatki[i].getTip()=='k' && podatki[i].getOdhodniDohodni()==1){
							stevilka=podatki[i].getStevilka();
							operaterSt=Operater.vrniOperaterja(stevilka);
							
							if(operaterSt==operaterUporanika){
								enota+=podatki[i].getTrajanje();
							}
						}
						
						break;
					case 't':
						if(podatki[i].getTip()=='k' && podatki[i].getOdhodniDohodni()==1){
							stevilka=podatki[i].getStevilka();
							operaterSt=Operater.vrniOperaterja(stevilka);
							
							if(operaterSt!=operaterUporanika && operaterSt!=Operater.STACIONARNA){
								enota+=podatki[i].getTrajanje();
							}
						}
						
						break;
				}			
				
				if(i<podatki.length){
					i++;
				}
			}
			else{
				
				k.dodajEnoto(enota);
				
				dan++;
				prejsniDan.setTime(dnevnaMeja.getTime());
				dnevnaMeja.setTime(dnevnaMeja.getTime()+(60*60*24*1000));
				
				enota=0;
				
			}
			
		}while(dan<stDni);
		
	}
	
	private int[] simulirajPodatke(){
		int[] sp=new int[5];
		sp[0]=0; // SMS, MMS
		sp[1]=0; // NET
		sp[2]=0; // Klici Domace
		sp[3]=0; // Klici Tuje
		sp[4]=0; // Klici Stac
		
		GregorianCalendar gc=new GregorianCalendar(leto, mesec, 1);
		int dniMeseca=gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		
		for (int i = stDni; i < dniMeseca; i++) {
			sp[0]+=simulirajEnoto(klasSmsMms);
			sp[1]+=simulirajEnoto(klasNet);
			sp[2]+=simulirajEnoto(klasKliciDomace);
			sp[3]+=simulirajEnoto(klasKliciTuje);
			sp[4]+=simulirajEnoto(klasKliciStac);
		}
		
		return sp;
	}
	
	private int simulirajEnoto(Klasifikator k){
		
		int index=0;
		double utez=0.0;
		double randProcent=Math.random();
		double [] procenti=k.procenti();
		int [][] razporeditevEnot=k.razporeditevEnot();
		
		for (int i = 0; i < procenti.length; i++) {
			
			if(randProcent>=utez && randProcent<utez+procenti[i]){
				index=i;
				break;
			}
			
			utez+=procenti[i];
		}
				
		return nakljucnaVrednostObsega(razporeditevEnot[index][0], razporeditevEnot[index][1]);
	}
	
	private int nakljucnaVrednostObsega(int min,int max){
		if(min==max){
			return min;
		}
		return min + (int)(Math.random() * ((max - min) + 1));
	}
	
	private int[] prestejPodatke(){
		int[] dp=new int[5];
		dp[0]=0; // SMS, MMS
		dp[1]=0; // NET
		dp[2]=0; // Klici Domace
		dp[3]=0; // Klici Tuje
		dp[4]=0; // Klici Stac
		
		int i=podatki.length-1;
		
		int stevilka;
		int operaterSt;
		
		do{
			
			switch (podatki[i].getTip()) {
				case 's':
					if(podatki[i].getOdhodniDohodni()==1){
						dp[0]++;
					}
					break;
				case 'm':
					if(podatki[i].getOdhodniDohodni()==1){
						dp[0]++;
					}
					break;
				case 'n':
					dp[1]+=podatki[i].getKolicina();
					break;
				case 'k':
					if(podatki[i].getOdhodniDohodni()==1){
						
						stevilka=podatki[i].getStevilka();
						operaterSt=Operater.vrniOperaterja(stevilka);
						
						if(operaterSt==Operater.STACIONARNA){
							dp[4]+=podatki[i].getTrajanje();
						}
						else if(operaterSt==operaterUporanika){
							dp[2]+=podatki[i].getTrajanje();
						}
						else{
							dp[3]+=podatki[i].getTrajanje();
						}
					}
					break;
			}				
			
			i--;
		}while(podatki[i].getDatum().getTime()>=meja.getTime());
		
		return dp;
	}
	
	private void primerjavaPodatkov(int[] sp, int[] dp){
		
		ArrayList<String> rezultati=new ArrayList<String>();
		
		double dovOdstop=nastavitveMesecneStatistike.dovoljenProcentOdstopanja;
		int minStEnot=nastavitveMesecneStatistike.minimalnoSteviloEnot;
		
		double procOdstSmsMms=procentOdstopanja(dp[0], sp[0]);
		if(dovOdstop<procOdstSmsMms && dp[0]>minStEnot){
			rezultati.add(String.format("SMS/MMS: Simulirana poraba: %d enot. Poraba manj�a za %.2f%%.", steviloSmsMms+sp[0], procOdstSmsMms));
		}
		
		double procOdstNet=procentOdstopanja(dp[1], sp[1]);
		if(dovOdstop<procOdstNet && (dp[1]/1024)>minStEnot){
			rezultati.add(String.format("Internet: Simulirana poraba: %d enot. Poraba manj�a za %.2f%%.", (kolicinaNet+sp[1])/1024, procOdstNet));
		}
		
		double procOdstkliciDomace=procentOdstopanja(dp[2], sp[2]);
		if(dovOdstop<procOdstkliciDomace  && (dp[3]/60)>minStEnot){
			rezultati.add(String.format("Klici (doma�e omre�je): Simulirana poraba: %d enot. Poraba manj�a za %.2f%%.", (trajanjeKliciDomace+sp[2])/60, procOdstkliciDomace));
		}
		
		double procOdstkliciTuje=procentOdstopanja(dp[3], sp[3]);
		if(dovOdstop<procOdstkliciTuje  && (dp[3]/60)>minStEnot){
			rezultati.add(String.format("Klici (tuje omre�je): Simulirana poraba: %d enot. Poraba manj�a za %.2f%%.", (trajanjeKliciTuje+sp[3])/60, procOdstkliciTuje));
		}
		
		double procOdstkliciStac=procentOdstopanja(dp[4], sp[4]);
		if(dovOdstop<procOdstkliciStac && (dp[4]/60)>minStEnot){
			rezultati.add(String.format("Klici (stacionarno omre�je): Simulirana poraba: %d enot. Poraba manj�a za %.2f%%.", (trajanjeKliciStac+sp[4])/60, procOdstkliciStac));
		}
		
		rezultat.nastaviVrednosti("mesecnaStatistika", rezultati.toArray(new String[0]));
	}
	
	private double procentOdstopanja(int d, int s){
		
		if(d==s){
			return 0.0;
		}
		
		if(d==0 || s==0){
			return 100.0;
		}
		
		int razlika=s-d;
		
		if(razlika>0){
			double povprecje=(d+s)/2.0;
			
			double procentRazlike=razlika/povprecje*100.0;
			
			return procentRazlike;
		}
		
		return 0.0;
	}
	
	public Rezultat vrniRezultate(){
		return rezultat;
	}
	
	public void koncaj(){
		baza.zapriPovezavo();
		podatki=null;
	}
}
