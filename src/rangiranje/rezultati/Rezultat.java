package rangiranje.rezultati;

import java.util.HashMap;

import entitete.RangiranaStoritev;

public class Rezultat {
	
	HashMap<String, String> vrednosti;
	HashMap<String, String[]> tabeleVrednosti;
	HashMap<String, RangiranaStoritev[]> tabelaStoritev;

	public Rezultat() {
		
		vrednosti=new HashMap<String, String>();
		tabeleVrednosti=new HashMap<String, String[]>();
		tabelaStoritev=new HashMap<String, RangiranaStoritev[]>();
		
	}
	
	public void nastaviVrednost(String kljuc, String vrednost){
		vrednosti.put(kljuc, vrednost);
	}
	
	public String vrniVrednost(String kljuc){
		return vrednosti.get(kljuc);
	}
	
	public void nastaviVrednosti(String kljuc, String[] vrednosti){
		tabeleVrednosti.put(kljuc, vrednosti);
	}
	
	public String[] vrniVrednosti(String kljuc){
		return tabeleVrednosti.get(kljuc);
	}
	
	public void nastaviStoritve(String kljuc, RangiranaStoritev[] storitve){
		tabelaStoritev.put(kljuc, storitve);
	}
	
	public RangiranaStoritev[] vrniStoritve(String kljuc){
		return tabelaStoritev.get(kljuc);
	}

}
