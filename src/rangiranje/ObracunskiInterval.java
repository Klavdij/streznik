package rangiranje;

import java.util.HashMap;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import entitete.Podatek;
import orodja.Baza;
import orodja.nastavitve.Nastavitve;
import orodja.nastavitve.ObracunskiIntervalNastavitve;
import rangiranje.rezultati.Rezultat;

public class ObracunskiInterval {
	
	private ObjectId idUporabnika;
	
	private Baza baza;
	
	private ObracunskiIntervalNastavitve nastavitveObracunskegaIntervala;
	
	private Podatek[] podatki;
	
	private HashMap<String, Integer> vrednostiIntervalov;
	
	private final int vrednostiKlica=60;
	
	private Rezultat rezultat;

	public ObracunskiInterval(String idUporabnika) {
		
		this.idUporabnika = new ObjectId(idUporabnika);
		
		baza=new Baza();
		
		Nastavitve nastavitve=new Nastavitve();
		nastavitveObracunskegaIntervala=nastavitve.vrniNastavitveObracunskegaIntervala();
		
		vrednostiIntervalov=new HashMap<String, Integer>();
		
		podatki=naloziPodatke();
		
		rezultat=new Rezultat();
		
		BasicDBObject storitev=baza.dobiStoritevUporabnika(this.idUporabnika);
		String intervalUporabnika=storitev.getString("obracunski_interval_klic")+"/"+storitev.getString("obracunski_interval_enota");
		
		rezultat.nastaviVrednost("intervalUporabnika", intervalUporabnika);
	}
	
	private Podatek[] naloziPodatke(){
		Podatek [] p=null;
		
		BasicDBList pList;
		BasicDBObject objekt;
		
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUporabnika);
		pogoji.append("tip", "k");
		pogoji.append("odhodni_dohodni", 1);
		
		pList=baza.dobiPodatke(Podatek.ZBIRKA, pogoji, null);
		
		int stKlicev=nastavitveObracunskegaIntervala.steviloKlicev;
		
		if(pList!=null && pList.size()>0 && pList.size()>stKlicev){
			
			int razlika=pList.size()-stKlicev;
			
			int zacetek=(int)(Math.random() * razlika);
			
			p=new Podatek[stKlicev];
			
			for (int i = 0; i < p.length; i++) {
				objekt=(BasicDBObject) pList.get(zacetek+i);
				
				p[i]=new Podatek(objekt);
			}
		}
		
		return p;
	}
	
	public void obdelava(){
		
		if(podatki!=null){
			
			izracunajVrednostiIntervalov();
			
		}
		
	}
	
	private void izracunajVrednostiIntervalov(){
		
		int trajanje;
		int [][] intervali=nastavitveObracunskegaIntervala.obracunskiIntervali;
		String[] imenaIntervalov=nastavitveObracunskegaIntervala.imenaObracunskihIntervalov;
		int vrednostKlica;
		
		for (int i = 0; i < podatki.length; i++) {
			trajanje=podatki[i].getTrajanje();
			
			for (int j = 0; j < intervali.length; j++) {
				
				vrednostKlica=izracunajVrednostKlica(trajanje,intervali[j]);
				
				if(vrednostiIntervalov.containsKey(imenaIntervalov[j])){
					vrednostiIntervalov.put(imenaIntervalov[j], vrednostiIntervalov.get(imenaIntervalov[j])+vrednostKlica);
				}
				else{
					vrednostiIntervalov.put(imenaIntervalov[j], vrednostKlica);
				}
			}
		}
	}
	
	private int izracunajVrednostKlica(int trajanje,int[] interval){
		int vrednost=vrednostiKlica;
		
		int ostaleMinute=trajanje-interval[0];
					
		while(ostaleMinute>0){
			vrednost+=interval[1];
			ostaleMinute-=interval[1];
		}
		
		return vrednost;
	}
	
	public Rezultat vrniRezultate(){
				
		String imena="";
		String vrednosti="";		
		
		for ( String keySeznam : vrednostiIntervalov.keySet() ) {
			
			imena+=keySeznam+";";
			vrednosti+=vrednostiIntervalov.get(keySeznam)+";";

		}
		
		rezultat.nastaviVrednost("imenaIntervalov", imena);
		rezultat.nastaviVrednost("vrednostiIntervalov", vrednosti);

		return rezultat;
	}
	
	public void koncaj(){
		baza.zapriPovezavo();
		podatki=null;
	}
}
