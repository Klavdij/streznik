package rangiranje;

import java.text.DecimalFormat;
import java.util.HashSet;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import orodja.Baza;
import orodja.Gostovanje;
import orodja.nastavitve.Nastavitve;
import orodja.nastavitve.TujinaOdstopanjeNastavitve;
import rangiranje.rezultati.Rezultat;
import entitete.MesecniPodatkiTujina;
import entitete.TujinaPodatki;

public class Tujina {
	
	private ObjectId idUporabnika;
	
	private Baza baza;
	private Nastavitve nastavitve;
	private Gostovanje gostovanje;
	
	private TujinaOdstopanjeNastavitve tujinaNastavitve;
	
	private MesecniPodatkiTujina[] mesecniPodatkiTujina;
	private TujinaPodatki tujinaPodatki;
	
	private Rezultat rezultat;

	public Tujina(String idUporabnika) {
		
		this.idUporabnika = new ObjectId(idUporabnika);
		
		baza=new Baza();
		nastavitve=new Nastavitve();
		gostovanje=new Gostovanje();
		
		tujinaNastavitve=nastavitve.vrniNastavitveOdstopanjaTujine();
		
		tujinaPodatki=naloziTujinaPodatki();
		
		mesecniPodatkiTujina=naloziMesecnePodatkeTujina();
		
		rezultat=new Rezultat();
	}
	
	private TujinaPodatki naloziTujinaPodatki(){
		
		TujinaPodatki tp=null;
		
		BasicDBObject pogoj=new BasicDBObject("id_uporabnika", idUporabnika);
		
		BasicDBObject tpObjekt=baza.dobiEnega(TujinaPodatki.ZBIRKA, pogoj, null, null);
		
		if(tpObjekt!=null){
			tp=new TujinaPodatki(tpObjekt);
		}
		
		return tp;
	}
	
	private MesecniPodatkiTujina[] naloziMesecnePodatkeTujina(){
		
		MesecniPodatkiTujina[] mpt=null;
		
		BasicDBList mptList;
		BasicDBObject objekt;
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUporabnika);
		
		BasicDBObject sortiranje=new BasicDBObject("leto", 1);
		sortiranje.append("mesec", 1);
		
		if(tujinaPodatki!=null){
			
			pogoji.append(
					"_id",
					new BasicDBObject("$gte", baza.dobiIdObjekta(
							MesecniPodatkiTujina.ZBIRKA, idUporabnika,
							tujinaPodatki.getKoncnoLeto(),
							tujinaPodatki.getKoncniMesec())));
		}
		
		mptList=baza.dobiPodatke(MesecniPodatkiTujina.ZBIRKA, pogoji, sortiranje);
		
		if(mptList!=null && mptList.size()>0){
			
			mpt=new MesecniPodatkiTujina[mptList.size()];
			
			for (int i = 0; i < mptList.size(); i++) {
				objekt=(BasicDBObject) mptList.get(i);
				
				mpt[i]=new MesecniPodatkiTujina(objekt);
			}
			
			tujinaPodatki=new TujinaPodatki(idUporabnika, mpt[mptList.size()-1].getLeto(), mpt[mptList.size()-1].getMesec());
		}
		
		return mpt;
	}
	
	public void obdelava(){
		
		if(mesecniPodatkiTujina!=null){
			
			pridobiPodatkeZaTujino();
			
			lastnositPodatkovZaTujino();
			
		}
		
	}
	
	private void pridobiPodatkeZaTujino(){
		
		HashSet<String> drzave=new HashSet<String>();
		tujinaPodatki.setSteviloOdstopanj(mesecniPodatkiTujina.length);
		
		for (int i = 0; i < mesecniPodatkiTujina.length; i++) {
			
			if(!drzave.contains(mesecniPodatkiTujina[i].getDrzava())){
				drzave.add(mesecniPodatkiTujina[i].getDrzava());
			}
			
			tujinaPodatki.addSkupnaCena(gostovanje.vrniCenoEnot(
					mesecniPodatkiTujina[i].getDrzava(), 'o',
					mesecniPodatkiTujina[i].getKolicinaKliciOdhodniMin()));
			tujinaPodatki.addSkupnaCena(gostovanje.vrniCenoEnot(
					mesecniPodatkiTujina[i].getDrzava(), 'd',
					mesecniPodatkiTujina[i].getKolicinaKliciDohodniMin()));
			tujinaPodatki.addSkupnaCena(gostovanje.vrniCenoEnot(
					mesecniPodatkiTujina[i].getDrzava(), 's',
					mesecniPodatkiTujina[i].getKolicinaSmsMms()));
			tujinaPodatki.addSkupnaCena(gostovanje.vrniCenoEnot(
					mesecniPodatkiTujina[i].getDrzava(), 'n',
					mesecniPodatkiTujina[i].getKolicinaNetMB()));
			
		}
		
		tujinaPodatki.addSteviloDrzav(drzave.size());
	}
	
	private void lastnositPodatkovZaTujino(){
		
		int stOdstopanj=tujinaPodatki.getSteviloOdstopanj();
		int stDrzav=tujinaPodatki.getSteviloDrzav();
		double mesecnaCena=tujinaPodatki.getPovprecnaMesecnaCena();
		
		if(stOdstopanj>tujinaNastavitve.stOdstopanj){
			
			if(mesecnaCena>tujinaNastavitve.cena){
				if(stDrzav>tujinaNastavitve.stDrzav){
					
					rezultat.nastaviVrednost("nasvet", "uporabite opcijo za tujino");
				}
				else{
					rezultat.nastaviVrednost("nasvet", "uporabite tujega operaterja");
				}
			}
		}
		else{
			
			if(mesecnaCena>tujinaNastavitve.cena){
				
				if(stDrzav>tujinaNastavitve.stDrzav){
					
					rezultat.nastaviVrednost("nasvet", "uporabite opcijo za tujino");
				}
				else{
					rezultat.nastaviVrednost("nasvet", "uporabite plačniške kartice");
				}
			}
		}
	}
	

	public Rezultat vrniRezultate(){
		
		if(tujinaPodatki!=null){
			DecimalFormat df = new DecimalFormat("#,###.##");
			
			rezultat.nastaviVrednost("stOdstopanj", tujinaPodatki.getSteviloOdstopanj()+"");
			rezultat.nastaviVrednost("stDrzav", tujinaPodatki.getSteviloDrzav()+"");
			rezultat.nastaviVrednost("skupnaCena", df.format(tujinaPodatki.getSkupnaCena()));
			rezultat.nastaviVrednost("mesecnaCena", df.format(tujinaPodatki.getPovprecnaMesecnaCena()));
		}
		else{
			rezultat.nastaviVrednost("stOdstopanj", "0");
			rezultat.nastaviVrednost("stDrzav", "0");
			rezultat.nastaviVrednost("skupnaCena", "0.0");
			rezultat.nastaviVrednost("mesecnaCena", "0.0");
		}
		
		return rezultat;
	}
	
	public void koncaj(){
		baza.zapriPovezavo();
		mesecniPodatkiTujina=null;
	}

}
