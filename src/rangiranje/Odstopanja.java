package rangiranje;

import java.util.Date;
import java.util.GregorianCalendar;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import orodja.Baza;
import rangiranje.rezultati.Rezultat;
import entitete.Odstopanje;

public class Odstopanja {
	
	private ObjectId idUporabnika;
	
	private Baza baza;
	
	private Odstopanje[] odstopanja;
	private Odstopanje najvecjeOstopanje;
	
	private Rezultat rezultat;

	public Odstopanja(String idUporabnika) {
		
		this.idUporabnika = new ObjectId(idUporabnika);
		
		baza=new Baza();
		
		odstopanja=naloziOdstopanja();
		najvecjeOstopanje=null;
		
		rezultat=new Rezultat();
	}
	
	private Odstopanje[] naloziOdstopanja(){
		
		Odstopanje[] o=null;
		
		BasicDBList oList;
		BasicDBObject objekt;
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUporabnika);
		
		BasicDBObject sortiranje=new BasicDBObject("leto", 1);
		sortiranje.append("mesec", 1);
		
		oList=baza.dobiPodatke(Odstopanje.ZBIRKA, pogoji, sortiranje);
		
		if(oList!=null && oList.size()>0){
			
			o=new Odstopanje[oList.size()];
			
			for (int i = 0; i < oList.size(); i++) {
				objekt=(BasicDBObject) oList.get(i);
				
				o[i]=new Odstopanje(objekt);
			}
		}
		
		return o;
	}
	
	public void obdelava(){
		
		if(odstopanja!=null){
			
			poisciNajvecjeOdstopanje();
			
			ponovnoRangirajStoritve();
		}
		
	}
	
	private void poisciNajvecjeOdstopanje(){
		
		int sumaOdstopanja;
		int maxOdstopanja=Integer.MIN_VALUE;
		int maxIndex=0;
		
		for (int i = 0; i < odstopanja.length; i++) {
			
			sumaOdstopanja= odstopanja[i].getOdstopanjeKliciDomace()
					+ odstopanja[i].getOdstopanjeKliciStac()
					+ odstopanja[i].getOdstopanjeKliciTuje()
					+ odstopanja[i].getOdstopanjeNet()
					+ odstopanja[i].getOdstopanjeSmsMms();
			
			if(sumaOdstopanja>maxOdstopanja){
				maxOdstopanja=sumaOdstopanja;
				maxIndex=i;
			}

		}
		
		najvecjeOstopanje=odstopanja[maxIndex];
	}
	
	private void ponovnoRangirajStoritve(){
		
		Odstopanje zadnjeOdstopanje=odstopanja[odstopanja.length-1];
		
		if(!najvecjeOstopanje.equals(zadnjeOdstopanje)){
			
			Date zacetek=new GregorianCalendar(najvecjeOstopanje.getLeto(), najvecjeOstopanje.getMesec(), 1).getTime();
			Date konec=new GregorianCalendar(zadnjeOdstopanje.getLeto(), zadnjeOdstopanje.getMesec(), 1).getTime();
			
			RangiranjeStoritev rangiranje=new RangiranjeStoritev(idUporabnika, zacetek, konec);
			rangiranje.ponovnoRangiraj();
			rezultat=rangiranje.vrniRezultate();
			rangiranje.koncaj();
		}
		
	}
	
	public Rezultat vrniRezultate(){

		if(najvecjeOstopanje!=null){
			rezultat.nastaviVrednost("datumOdstopanja", (najvecjeOstopanje.getMesec()+1)+". "+najvecjeOstopanje.getLeto());
		}
		else{
			rezultat.nastaviVrednost("datumOdstopanja", "0");
		}
		
		return rezultat;
	}
	
	public void koncaj(){
		baza.zapriPovezavo();
		odstopanja=null;
	}
}
