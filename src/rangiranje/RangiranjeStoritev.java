package rangiranje;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import entitete.MesecniPodatki;
import entitete.MesecniPodatkiTujina;
import entitete.MesecnoPovprecje;
import entitete.Odstopanje;
import entitete.RangiraneStoritve;
import entitete.RezultatStroskov;
import entitete.SeznamMesecniPodatkiTujina;
import entitete.Storitev;
import orodja.Baza;
import orodja.Lokacija;
import orodja.Operater;
import orodja.nastavitve.Nastavitve;
import orodja.nastavitve.OdstopanjeNastavitve;
import rangiranje.rezultati.Rezultat;

public class RangiranjeStoritev {
	
	private ObjectId idUporabnika;
	private String drzavaUporabnika;
	private int operaterUporanika;
	
	private Baza baza;
	private Lokacija lokacija;
	
	private OdstopanjeNastavitve nastavitveOdstopanja;
	
	private MesecniPodatki [] mesecniPodatki;
	private MesecnoPovprecje mesecnoPovprecje;
	private SeznamMesecniPodatkiTujina seznamPodatkiTujina;
	private ArrayList<Odstopanje> odstopanja;
	
	private RangiraneStoritve rangiraneStoritve;
	
	private Date zacetek;
	private Date konec;
	private int mesecev;
	
	private boolean shraniVBazo;
	
	private Rezultat rezultat;
	
	public RangiranjeStoritev(String idUporabnika) {
				
		this.idUporabnika = new ObjectId(idUporabnika);
		
		baza=new Baza();
		lokacija=new Lokacija();
		
		Nastavitve nastavitve=new Nastavitve();		
		nastavitveOdstopanja=nastavitve.vrniNastavitveOdstopanja();
		
		drzavaUporabnika=pridobiDrzavoUporabnika();
		operaterUporanika=pridobiOperaterjaUporabnika();
		
		mesecev=pridobiObseg();
		
		mesecniPodatki=new MesecniPodatki[mesecev];
		seznamPodatkiTujina=new SeznamMesecniPodatkiTujina(this.idUporabnika);
		odstopanja=new ArrayList<Odstopanje>();
		
		rangiraneStoritve=null;
		
		shraniVBazo=true;
		
		ustvariRezultat();
	}
	
	public RangiranjeStoritev(ObjectId idUporabnika, Date zacetek, Date konec) {
		this.idUporabnika = idUporabnika;
		
		this.zacetek=zacetek;
		this.konec=konec;
		
		baza=new Baza();
		
		mesecev=razlikaVMesecih()+1;
		
		rangiraneStoritve=null;
		
		shraniVBazo=false;
		
		ustvariRezultat();
	}
	
	private void ustvariRezultat(){
		rezultat=new Rezultat();
		
		BasicDBObject storitev=baza.dobiStoritevUporabnika(this.idUporabnika);		
		
		rezultat.nastaviVrednost("idStoritveUporabnika", storitev.getString("_id"));
	}
	
	/**
	 * Nastavi zacetek in konec obdelave, ter MesecnoPovprecje ce obstaja
	 * @return Vrne stevilo mesecev za obdelavo
	 */
	private int pridobiObseg(){
		
		Date prviDatum;
		Date zadnjiDatum;
		
		BasicDBObject pogoj=new BasicDBObject("id_uporabnika", idUporabnika);
		BasicDBObject mpObject=baza.dobiEnega(MesecnoPovprecje.ZBIRKA, pogoj, null, null);
		
		if(mpObject!=null){
			
			mesecnoPovprecje=new MesecnoPovprecje(mpObject);
			
			prviDatum=mesecnoPovprecje.getKonec();
			zacetek=nastaviDatum(prviDatum, 0);
			
			zadnjiDatum=baza.dobiEnega("podatki", pogoj, null, new BasicDBObject("datum", -1)).getDate("datum");
			konec=nastaviDatum(zadnjiDatum, 0);
		}
		else{
			prviDatum=baza.dobiEnega("podatki", pogoj, null, new BasicDBObject("datum", 1)).getDate("datum");
			zadnjiDatum=baza.dobiEnega("podatki", pogoj, null, new BasicDBObject("datum", -1)).getDate("datum");
			
			zacetek=nastaviDatum(prviDatum, 1);
			konec=nastaviDatum(zadnjiDatum, 0);
			
			mesecnoPovprecje=null;
		}
		
		return razlikaVMesecih();
	}
	
	/**
	 * Izracuna razliko v mesecih med zacetkom in koncem
	 * @return stevilo mesecev
	 */
	private int razlikaVMesecih(){
		GregorianCalendar gcZ = new GregorianCalendar();
		gcZ.setTime(zacetek);
		GregorianCalendar gcK = new GregorianCalendar();
		gcK.setTime(konec);

		int razlikaLeta = gcK.get(GregorianCalendar.YEAR) - gcZ.get(GregorianCalendar.YEAR);
		
		int razlikaMeseci = razlikaLeta * 12 + gcK.get(GregorianCalendar.MONTH) - gcZ.get(GregorianCalendar.MONTH);
		
		return razlikaMeseci>0?razlikaMeseci:0;
	}
	
	/**
	 * Nastavi zacetni oz. koncni datum
	 * @param input Datum iz baze
	 * @param zamik Dodaj oz. odstrani mesec
	 * @return Zacetni oz. koncni datum
	 */
	private Date nastaviDatum(Date input, int zamik){
		GregorianCalendar gc=new GregorianCalendar();
		
		gc.setTime(input);
		
		GregorianCalendar gcOut=new GregorianCalendar(gc.get(GregorianCalendar.YEAR),gc.get(GregorianCalendar.MONTH)+zamik,1);
		
		return gcOut.getTime();
	}
	
	/**
	 * Iz baze preberemo drzavo uprabnika
	 * @return Drzavo uporabnika
	 */
	private String pridobiDrzavoUporabnika(){
		BasicDBObject uporabnik=baza.dobiEnega("uporabnik", new BasicDBObject("_id", idUporabnika),null,null);
		return ((BasicDBObject)uporabnik.get("naslov")).getString("drzava");
	}
	
	/**
	 * Iz baze prebere operaterja uporabnika
	 * @return operaterja
	 */
	private int pridobiOperaterjaUporabnika(){
		BasicDBObject storitev=baza.dobiStoritevUporabnika(idUporabnika);
		return Operater.vrniOperaterja(storitev.getString("operater"));
	}
	
	public void rangiraj(){
		
		// Sestej enote po mesecih, naredi skupno povprecje in podatke shrani v bazo
		sestejEnote();
		
		// Povprecje primerjaj s storitvami
		rangiranjeStoritev();
		
	}
	
	public void ponovnoRangiraj(){
		
		// Prebere podatke iz baze
		naloziPodatkeIzBaze();
		
		// Izracuna povprecje
		izracunajPovprecje();
		
		// Povprecje primerjaj s storitvami
		rangiranjeStoritev();
		
	}
	
	/**
	 * Nalozi mesecne podatke iz baze
	 */
	private void naloziPodatkeIzBaze(){
		
		mesecniPodatki=null;
		
		BasicDBList mpList;
		BasicDBObject objekt;
		
		GregorianCalendar z=new GregorianCalendar();
		z.setTime(zacetek);
		int leto=z.get(GregorianCalendar.YEAR);
		int mesec=z.get(GregorianCalendar.MONTH);
		
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUporabnika);

		pogoji.append("_id",new BasicDBObject("$gte", baza.dobiIdObjekta(MesecniPodatki.ZBIRKA, idUporabnika, leto, mesec)));
		
		BasicDBObject sortiranje=new BasicDBObject("leto", 1);
		sortiranje.append("mesec", 1);
		
		mpList=baza.dobiPodatke(MesecniPodatki.ZBIRKA, pogoji, sortiranje);

		if(mpList!=null && mpList.size()>0){
			
			mesecniPodatki=new MesecniPodatki[mpList.size()];
			
			for (int i = 0; i < mpList.size(); i++) {
				objekt=(BasicDBObject) mpList.get(i);
				
				mesecniPodatki[i]=new MesecniPodatki(objekt);
			}
		}
	}
	
	/**
	 * Izracuna povprecje iz mesecnih podatkov
	 */
	private void izracunajPovprecje(){
		if(mesecniPodatki!=null){
			
			for (int i = 0; i < mesecniPodatki.length; i++) {
				
				if(mesecnoPovprecje==null){
					mesecnoPovprecje=new MesecnoPovprecje(mesecniPodatki[i], zacetek, konec);
				}
				else{		
					mesecnoPovprecje.dodajKolicine(mesecniPodatki[i]);
				}
			}
		}
	}
	
	/**
	 * Sestej enote po mesecih, naredi skupno povprecje in podatke shrani v bazo
	 */
	private void sestejEnote(){
		
		if(mesecev>0){
			
			BasicDBObject podatek;
			
			double lokacijaX;
			double lokacijaY;
			String drzava;
			
			char tip;
			int odhodni;
			int stevilka;
			int operaterSt;
			
			int leto;
			int mesec;
			
			int smsMms=0;
			int kliciDomace=0;
			int kliciTuje=0;
			int kliciStac=0;
			int net=0;
			
			BasicDBList listPodatkov;
			BasicDBObject pogoj=new BasicDBObject("id_uporabnika", idUporabnika);
			
			for (int i = 0; i < mesecniPodatki.length; i++) {
				
				GregorianCalendar from=new GregorianCalendar();
				from.setTime(zacetek);
				int month=from.get(GregorianCalendar.MONTH);
				from.set(GregorianCalendar.MONTH, month+i);
				
				int days=from.getActualMaximum(Calendar.DAY_OF_MONTH);
				
				leto=from.get(GregorianCalendar.YEAR);
				mesec=from.get(GregorianCalendar.MONTH);
				
				GregorianCalendar to=new GregorianCalendar(leto,mesec,days,0,0,0);
				
				pogoj.append("datum", new BasicDBObject("$gte", from.getTime()).append("$lte", to.getTime()));
				
				listPodatkov=baza.dobiPodatke("podatki", pogoj, null);
				
				for (int j = 0; j < listPodatkov.size(); j++) {
					
					podatek=(BasicDBObject)listPodatkov.get(j);
					
					lokacijaX=podatek.getDouble("lokacija_x");
					lokacijaY=podatek.getDouble("lokacija_y");
					
					drzava=lokacija.getCountry(lokacijaX, lokacijaY);
					
					if(drzava.equals(drzavaUporabnika)){
						
						tip=podatek.getString("tip").charAt(0);
						odhodni=podatek.getInt("odhodni_dohodni");
						
						switch (tip) {
						case 's':
							if(odhodni==1){
								smsMms++;
							}
							break;
						case 'm':
							if(odhodni==1){
								smsMms++;
							}
							break;
						case 'n':
							net+=podatek.getInt("kolicina");
							break;
						case 'k':
							if(odhodni==1){
								
								stevilka=podatek.getInt("stevilka");
								operaterSt=Operater.vrniOperaterja(stevilka);
								
								if(operaterSt==Operater.STACIONARNA){
									kliciStac+=podatek.getInt("trajanje");
								}
								else if(operaterSt==operaterUporanika){
									kliciDomace+=podatek.getInt("trajanje");
								}
								else{
									kliciTuje+=podatek.getInt("trajanje");
								}
							}
							break;
						}				
						
					}
					else{
						// Obravnavamo tujino
						obravnavajPodatkeIzTujine(podatek,leto,mesec,drzava);
					}
					
				}
				
				mesecniPodatki[i]=new MesecniPodatki(idUporabnika, leto, mesec, kliciDomace, kliciTuje, kliciStac, smsMms, net);
				
				smsMms=0;
				kliciDomace=0;
				kliciTuje=0;
				kliciStac=0;
				net=0;
				
				if(mesecnoPovprecje==null){
					mesecnoPovprecje=new MesecnoPovprecje(mesecniPodatki[i], zacetek, konec);
				}
				else{
					// Ugotavljanje odstopanja
					preveriOdstopanje(mesecnoPovprecje, mesecniPodatki[i],nastavitveOdstopanja,leto,mesec);
					
					mesecnoPovprecje.dodajKolicine(mesecniPodatki[i]);
				}
				
			}
			
			mesecnoPovprecje.setKonec(konec);
			
			// Shranimo v bazo
			shraniPodatkeVBazo();
		}
	}
	
	/**
	 * Shrani podatke glede na leto, mesec in drzavo
	 * @param podatek Trenutni mesecni podatki
	 * @param leto Leto podatkov
	 * @param mesec Mesec podatkov
	 * @param drzava Drzava podatkov
	 */
	private void obravnavajPodatkeIzTujine(BasicDBObject podatek, int leto, int mesec,String drzava){
		
		char tip=podatek.getString("tip").charAt(0);
		int odhodni=podatek.getInt("odhodni_dohodni");
		
		MesecniPodatkiTujina mpt;
		
		mpt=seznamPodatkiTujina.vrniMesecniPodatekZaTujino(leto, mesec, drzava);
		
		switch (tip) {
		case 's':
			if(odhodni==1){
				mpt.addKolicinaSmsMms();
			}
			break;
		case 'm':
			if(odhodni==1){
				mpt.addKolicinaSmsMms();
			}
			break;
		case 'n':
			mpt.addKolicinaNet(podatek.getInt("kolicina"));
			break;
		case 'k':
			if(odhodni==1){
				mpt.addKolicinaKliciOdhodni(podatek.getInt("trajanje"));
			}
			else{
				mpt.addKolicinaKliciDohodni(podatek.getInt("trajanje"));
			}
			break;
		}				
		
	}
	
	/**
	 * Preverimo oddstopanje podameznih podatkov in jih zabelezimo ce presegajo prag
	 * @param mesecnoPovprecje Povprecje dosedanjih podatkov
	 * @param mesecniPodatki Podatki trenutnega meseca
	 * @param nastavitveOdstopanja Nastavitve velikosti pragov
	 */
	private void preveriOdstopanje(MesecnoPovprecje mesecnoPovprecje, MesecniPodatki  mesecniPodatki, OdstopanjeNastavitve nastavitveOdstopanja,int leto, int mesec){
		
		boolean jeOdstopanje=false;
		
		int procentOdstopanjaKliciDomace=0;
		int procentOdstopanjaKliciTuje=0;
		int procentOdstopanjaKliciStac=0;
		int procentOdstopanjaSmsMms=0;
		int procentOdstopanjaNet=0;
		
		int razlika;
		int procentOdstopanja;
		
		razlika=Math.abs(mesecnoPovprecje.getKolicinaKliciDomace()-mesecniPodatki.getKolicinaKliciDomace());
		procentOdstopanja=(int)Math.round(100.0/mesecnoPovprecje.getKolicinaKliciDomace()*razlika);
		if(procentOdstopanja>nastavitveOdstopanja.kliciDomace){
			jeOdstopanje=true;
			procentOdstopanjaKliciDomace=procentOdstopanja;
		}
		
		razlika=Math.abs(mesecnoPovprecje.getKolicinaKliciTuje()-mesecniPodatki.getKolicinaKliciTuje());
		procentOdstopanja=(int)Math.round(100.0/mesecnoPovprecje.getKolicinaKliciTuje()*razlika);
		if(procentOdstopanja>nastavitveOdstopanja.kliciTuje){
			jeOdstopanje=true;
			procentOdstopanjaKliciTuje=procentOdstopanja;
		}
		
		razlika=Math.abs(mesecnoPovprecje.getKolicinaKliciStac()-mesecniPodatki.getKolicinaKliciStac());
		procentOdstopanja=(int)Math.round(100.0/mesecnoPovprecje.getKolicinaKliciStac()*razlika);
		if(procentOdstopanja>nastavitveOdstopanja.kliciStac){
			jeOdstopanje=true;
			procentOdstopanjaKliciStac=procentOdstopanja;
		}
		
		razlika=Math.abs(mesecnoPovprecje.getKolicinaSmsMms()-mesecniPodatki.getKolicinaSmsMms());
		procentOdstopanja=(int)Math.round(100.0/mesecnoPovprecje.getKolicinaSmsMms()*razlika);
		if(procentOdstopanja>nastavitveOdstopanja.smsMms){
			jeOdstopanje=true;
			procentOdstopanjaSmsMms=procentOdstopanja;
		}
		
		razlika=Math.abs(mesecnoPovprecje.getKolicinaNet()-mesecniPodatki.getKolicinaNet());
		procentOdstopanja=(int)Math.round(100.0/mesecnoPovprecje.getKolicinaNet()*razlika);
		if(procentOdstopanja>nastavitveOdstopanja.net){
			jeOdstopanje=true;
			procentOdstopanjaNet=procentOdstopanja;
		}
		
		if(jeOdstopanje){
			odstopanja.add(new Odstopanje(idUporabnika, 
					leto, 
					mesec, 
					procentOdstopanjaKliciDomace, 
					procentOdstopanjaKliciTuje, 
					procentOdstopanjaKliciStac, 
					procentOdstopanjaSmsMms, 
					procentOdstopanjaNet));
		}
	}
	
	/**
	 * Shrani mesecno povprecje, mesecne podatke, odstopanja in podatke iz tujine v bazo.
	 */
	private void shraniPodatkeVBazo(){
		if(shraniVBazo){
			for (int i = 0; i < mesecniPodatki.length; i++) {
				baza.shraniStandardnePodatke(mesecniPodatki[i]);
			}
			
			for (int i = 0; i < odstopanja.size(); i++) {
				baza.shraniStandardnePodatke(odstopanja.get(i));
			}
			
			baza.shraniStandardnePodatke(mesecnoPovprecje);
			
			if(seznamPodatkiTujina.getSize()>0){
				seznamPodatkiTujina.shraniVBazo(baza);
			}
		}
	}

	/**
	 * Doloci ceno ce bi uporabnik imel izbrano storitev glede na najegovo povprecno porabo 
	 */
	private void rangiranjeStoritev(){
		
		if(mesecev>0){
			
			int skupnaKol;
			int smsMmsKol;
			int netKol;
			int kliciDomaceKol;
			int kliciTujeStacKol;
			
			RezultatStroskov rs;
			
			double mesecnaNarocnina;
			double dodatniStroski;
			
			BasicDBList storitve=baza.dobiPodatke(Storitev.ZBIRKA, null, null);
			
			Storitev[] s=new Storitev[storitve.size()];
			
			rangiraneStoritve=new RangiraneStoritve(idUporabnika, storitve.size());

			for (int i = 0; i < s.length; i++) {
				
				s[i]=new Storitev((BasicDBObject)storitve.get(i));
				
				mesecnaNarocnina=s[i].getMesecnaNarocnina();
				dodatniStroski=0.0;
				
				skupnaKol=s[i].getZakupljenaKolicinaSkupna();
				smsMmsKol=s[i].getZakupljenaKolicinaSmsMms();
				netKol=s[i].getZakupljenaKolicinaNet();
				kliciDomaceKol=s[i].getZakupljenaKolicinaKliciDomace();
				kliciTujeStacKol=s[i].getZakupljenaKolicinaKliciTujeStac();
				
				rs=izracunajDodatneStroske(skupnaKol,netKol,'n',s[i]);
				dodatniStroski+=rs.dodatniStroski;
				skupnaKol=rs.skupnaKolicina;
				
				rs=izracunajDodatneStroske(skupnaKol,smsMmsKol,'m',s[i]);
				dodatniStroski+=rs.dodatniStroski;
				skupnaKol=rs.skupnaKolicina;
				
				rs=izracunajDodatneStroske(skupnaKol,kliciDomaceKol,'d',s[i]);
				dodatniStroski+=rs.dodatniStroski;
				skupnaKol=rs.skupnaKolicina;
				
				rs=izracunajDodatneStroske(skupnaKol,kliciTujeStacKol,'t',s[i]);
				dodatniStroski+=rs.dodatniStroski;
				skupnaKol=rs.skupnaKolicina;
				kliciTujeStacKol=rs.kolicina;
				
				rs=izracunajDodatneStroske(skupnaKol,kliciTujeStacKol,'s',s[i]);
				dodatniStroski+=rs.dodatniStroski;
				
				mesecnaNarocnina+=dodatniStroski;
				
				rangiraneStoritve.dodajStoritev(s[i], mesecnaNarocnina);
			}
			
			if(shraniVBazo){
				baza.shraniStandardnePodatke(rangiraneStoritve);
			}
			
		}
		else{
			BasicDBObject pogoj=new BasicDBObject("id_uporabnika", idUporabnika);
			BasicDBObject rs=baza.dobiEnega(RangiraneStoritve.ZBIRKA, pogoj, null, null);
			if(rs!=null){
				rangiraneStoritve=new RangiraneStoritve(rs);
			}
		}
		
	}
	
	private RezultatStroskov izracunajDodatneStroske(int skupnaKol,int kolicina,char tip, Storitev s){
		int razlika;
		RezultatStroskov rs=new RezultatStroskov();
		
		if(skupnaKol>0){
			
			if(kolicina!=-1){
				
				razlika=skupnaKol-dobiKolicino(tip);
				
				if(razlika<0){
					rs.dodatniStroski=Math.abs(razlika)*dobiCenoEnote(tip, s);
				}
				else{
					rs.skupnaKolicina=razlika;
				}
				
			}
			else{
				rs.skupnaKolicina=skupnaKol;
			}
		}
		else{
			
			if(kolicina!=-1){
				
				razlika=kolicina-dobiKolicino(tip);
				
				if(razlika<0){
					rs.dodatniStroski=Math.abs(razlika)*dobiCenoEnote(tip, s);
				}
				else{
					rs.kolicina=razlika;
				}
			}
		}
		
		return rs;
	}
	
	private int dobiKolicino(char tip){
		switch (tip) {
		case 'n':
			return mesecnoPovprecje.getKolicinaNetMB();
		case 'm':
			return mesecnoPovprecje.getKolicinaSmsMms();
		case 'd':
			return mesecnoPovprecje.getKolicinaKliciDomaceMin();
		case 't':
			return mesecnoPovprecje.getKolicinaKliciTujeMin();
		case 's':
			return mesecnoPovprecje.getKolicinaKliciStacMin();
		default:
			return 0;
		}
	}
	
	private double dobiCenoEnote(char tip, Storitev s){
		switch (tip) {
		case 'n':
			return s.getCenaNaEnotoNet();
		case 'm':
			return s.getCenaNaEnotoSmsMms();
		case 'd':
			return s.getCenaNaEnotoKlicDomace();
		case 't':
			return s.getCenaNaEnotoKlicTuje();
		case 's':
			return s.getCenaNaEnotoKlicStac();
		default:
			return 0.0;
		}
	}
	
	public Rezultat vrniRezultate(){
		
		if(rangiraneStoritve!=null){
			rezultat.nastaviStoritve("rangiraneStoritve", rangiraneStoritve.vrniRangiraneStoritve());
		}		
		
		return rezultat;
	}
	
	public void koncaj(){
		baza.zapriPovezavo();
		lokacija=null;
		mesecniPodatki=null;
		seznamPodatkiTujina=null;
	}

}
