package rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import orodja.Baza;

@Path("/send/data")
public class PosljiPodatke {

	// This method is called if TEXT_PLAIN is request
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String plainTextDescription() {
		return "Spletna storitev za sprejem podatkov.";
	}

	// This method is called if XML is request
	@GET
	@Produces(MediaType.TEXT_XML)
	public String XMLDescription() {
		return "<?xml version=\"1.0\"?>" + "<service>Spletna storitev za sprejem podatkov." + "</service>";
	}

	// This method is called if HTML is request
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String htmlDescription() {
		return "<html> " + "<title>" + "Storitev" + "</title>"
				+ "<body><h1>" + "Spletna storitev za sprejem podatkov." + "</body></h1>" + "</html> ";
	}

	@POST
	public String prejmiPodatke(MultivaluedMap<String, String>params) {

		Baza b=new Baza();
				
		String emso=params.getFirst("emso");
		
		JsonParser parser = new JsonParser();
		JsonObject podatki=(JsonObject)parser.parse(params.getFirst("podatki"));
	
		b.shraniPodatke(emso, podatki);
		
		b.zapriPovezavo();
		
		return "{\"result\": \"success\"}";
	}

}