package rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import orodja.Baza;

@Path("/get/users/data")
public class PrejmiPodatke {

	// This method is called if TEXT_PLAIN is request
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String vrniUporabnike() {
		
		JsonArray uporabniki=new JsonArray();
		JsonObject uporabnik;
		
		Baza baza=new Baza();
		
		BasicDBList listUpo=baza.dobiPodatke("uporabnik", null, new BasicDBObject("ime", 1).append("priimek", 1));
		
		
		for (int i = 0; i < listUpo.size(); i++) {
			BasicDBObject o=(BasicDBObject) listUpo.get(i);
			
			uporabnik=new JsonObject();
			
			uporabnik.addProperty("imePrimek", o.getString("ime")+" "+o.getString("priimek"));
			uporabnik.addProperty("id", o.getString("_id"));
			uporabnik.addProperty("emso", o.getString("emso"));
			
			uporabniki.add(uporabnik);
		}
		
		baza.zapriPovezavo();
		
		return uporabniki.toString();
	}
	
}