﻿<%@ page import="procesi.PodatkiProces" %>
<% String idUpo=(String)session.getAttribute("id_uporabnika"); %>
<input type="hidden" id="uporabnik" value="<%= (String)session.getAttribute("id_uporabnika")%>"/>
<input type="hidden" id="st_upo" value="<%= PodatkiProces.stUporabnikov() %>"/>
<h1>Primerjava lastne porabe s povprečjem</h1>
<h2>SMS</h2>
<div id="graf_sms" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="sms" value="day">Dnevih
					<input type="radio" name="sms" value="week">Tednih 
					<input type="radio" name="sms" value="month" checked>Mesecih</div>
<script type="text/javascript">
	
	var uporabnik=$("#uporabnik").val();

	var st_upo=$("#st_upo").val();
	
	function sumAvg (arr) {
		var len = arr.length, 
			ret;
			
		// 1. it consists of nulls exclusively
		if (!len && arr.hasNulls) {
			ret = null;
		// 2. it has a length and real values
		} else if (len) {
			ret = 0;
			while (len--) {
				ret += arr[len];
			}
			ret=ret/st_upo;
		}
		// 3. it has zero length, so just return undefined 
		// => doNothing()
		
		return ret;
	}
</script>
<script type="text/javascript">

	$(function () { 
		var data_link="podatki.jsp?kategorija=statistika&tip=s&uporabnik="+uporabnik;
		var data_all_link="podatki.jsp?kategorija=primerjava&tip=s&uporabnik="+uporabnik;
		
		$.getJSON(data_link, function (data) {
			$.getJSON(data_all_link, function (data_all) {
				$('#graf_sms').highcharts('StockChart', {
					title: {
						text: 'Poraba sms-ov'
				    },
				    chart: {
			            backgroundColor:'transparent'
			        },
				    legend: {
				    	enabled: true
				    },
				    series: [{
				        name: 'Porabljenih sms-ov',
				        data: data,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: "sum",
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    },
				    {
				        name: 'Porabljenih sms-ov povprecje',
				        data: data_all,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: function (arr) {	return sumAvg (arr);},
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    }]
				});
			});
		});
	});
</script>
<h2>MMS</h2>
<div id="graf_mms" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="mms" value="day">Dnevih
					<input type="radio" name="mms" value="week">Tednih 
					<input type="radio" name="mms" value="month" checked>Mesecih</div>
<script type="text/javascript">

	$(function () { 
		var data_link="podatki.jsp?kategorija=statistika&tip=m&uporabnik="+uporabnik;
		var data_all_link="podatki.jsp?kategorija=primerjava&tip=m&uporabnik="+uporabnik;
		
		$.getJSON(data_link, function (data) {
			$.getJSON(data_all_link, function (data_all) {
				$('#graf_mms').highcharts('StockChart', {
					title: {
						text: 'Poraba mms-ov'
				    },
				    chart: {
			            backgroundColor:'transparent'
			        },
				    legend: {
				    	enabled: true
				    },
				    series: [{
				        name: 'Porabljenih mms-ov',
				        data: data,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: "sum",
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    },
				    {
				        name: 'Porabljenih mms-ov povprecje',
				        data: data_all,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: function (arr) {	return sumAvg (arr);},
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    }]
				});
			});
		});
	});
</script>
<h2>Klici</h2>
<div id="graf_klici" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="klici" value="day">Dnevih
					<input type="radio" name="klici" value="week">Tednih 
					<input type="radio" name="klici" value="month" checked>Mesecih</div>
<script type="text/javascript">

	$(function () { 
		var data_link="podatki.jsp?kategorija=statistika&tip=k&uporabnik="+uporabnik;
		var data_all_link="podatki.jsp?kategorija=primerjava&tip=k&uporabnik="+uporabnik;
		
		$.getJSON(data_link, function (data) {
			$.getJSON(data_all_link, function (data_all) {
				$('#graf_klici').highcharts('StockChart', {
					title: {
						text: 'Porabljene minute klicov'
				    },
				    chart: {
			            backgroundColor:'transparent'
			        },
				    legend: {
				    	enabled: true
				    },
				    series: [{
				        name: 'Porabljene minute',
				        data: data,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: "sum",
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    },
				    {
				        name: 'Porabljenih minut povprecje',
				        data: data_all,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: function (arr) {	return sumAvg (arr);},
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    }]
				});
			});
		});
	});
</script>
<h2>Internet</h2>
<div id="graf_net" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="net" value="day">Dnevih
					<input type="radio" name="net" value="week">Tednih 
					<input type="radio" name="net" value="month" checked>Mesecih</div>
<script type="text/javascript">

	$(function () {
		var data_link="podatki.jsp?kategorija=statistika&tip=n&uporabnik="+uporabnik;
		var data_all_link="podatki.jsp?kategorija=primerjava&tip=n&uporabnik="+uporabnik;
		
		$.getJSON(data_link, function (data) {
			$.getJSON(data_all_link, function (data_all) {
				$('#graf_net').highcharts('StockChart', {
					title: {
						text: 'Porabljeni megabajti'
				    },
				    chart: {
			            backgroundColor:'transparent'
			        },
				    legend: {
				    	enabled: true
				    },
				    series: [{
				        name: 'Porabljeni mB',
				        data: data,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: "sum",
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    },
				    {
				        name: 'Porabljenih mB povprecje',
				        data: data_all,
				        marker : {
							enabled : true,
							radius : 3
						},
				        dataGrouping: {
				            approximation: function (arr) {	return sumAvg (arr);},
				            enabled: true,
				            forced: true,
				            units: [['month',[1]]]
				        }
				    }]
				});
			});
		});
	});
	
	
	$.each(['sms', 'mms', 'klici', 'net'], function (i, type) {
		
	    $('input[name='+type+']').click(function () {
	    	
	    	var chart=$('#graf_'+type).highcharts();
	    	
	    	var value=$('input[name='+type+']:checked').val();
	    	
	        chart.series[0].update({
	        	dataGrouping: {
		            units: [[value,[1]]]
		        }
	        });
	        chart.series[1].update({
	        	dataGrouping: {
		            units: [[value,[1]]]
		        }
	        });
	    });
	});
</script>
