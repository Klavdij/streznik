﻿<%@page import="procesi.AnalizaProces"%>
<%@page import="entitete.RangiranaStoritev"%>
<%@page import="rangiranje.rezultati.Rezultat"%>
<%@page import="rangiranje.RangiranjeStoritev" %>
<%@page import="rangiranje.ObracunskiInterval"%>
<%@page import="rangiranje.MesecnaStatistika"%>
<%@page import="rangiranje.Odstopanja"%>
<%@page import="rangiranje.Tujina"%>
<%
String idUporabnika=(String)session.getAttribute("id_uporabnika");
%>
<h1>Storitve</h1>
<%

Rezultat rsRezultati=null;
Rezultat oRezultati=null;
RangiranaStoritev[] storitve=null;
RangiranaStoritev[] storitveOpti=null;
String idStoritveUporabnika=null;

RangiranjeStoritev rs=new RangiranjeStoritev(idUporabnika);
rs.rangiraj();
rsRezultati=rs.vrniRezultate();
rs.koncaj();

storitve=rsRezultati.vrniStoritve("rangiraneStoritve");
idStoritveUporabnika=rsRezultati.vrniVrednost("idStoritveUporabnika");

Odstopanja odstopanja=new Odstopanja(idUporabnika);
odstopanja.obdelava();
oRezultati=odstopanja.vrniRezultate();
odstopanja.koncaj();

storitveOpti=oRezultati.vrniStoritve("rangiraneStoritve");

String[] operaterji=AnalizaProces.vrniOperaterje();

String narocnina="vsi";
String[] narocninaChecked=new String[3];
narocninaChecked[0]="checked='checked'";

if(request.getParameter("narocnina")!=null){
	narocnina=request.getParameter("narocnina");
}

if(narocnina.equals("brez")){
	narocninaChecked[1]="checked='checked'";
}
else if(narocnina.equals("z")){
	narocninaChecked[2]="checked='checked'";
}

String[] operaterjiIzbrani=operaterji;
String[] operaterjiChecked=new String[operaterji.length];

if(request.getParameterValues("operater")!=null){
	operaterjiIzbrani = request.getParameterValues("operater");
}

for (int i = 0; i < operaterjiChecked.length; i++){
	
	if(AnalizaProces.vsebujeOperaterja(operaterji[i], operaterjiIzbrani)){
		operaterjiChecked[i]="checked='checked'";
	}
	else{
		operaterjiChecked[i]="";
	}
}

String prikaz="skrceno";
String[] prikazChecked=new String[2];
prikazChecked[0]="checked='checked'";

if(request.getParameter("prikaz")!=null){
	prikaz=request.getParameter("prikaz");
}

if(prikaz.equals("vse")){
	prikazChecked[1]="checked='checked'";
}

%>
<div class="storitve_ovoj">
	<div>
		<b>Filtri</b>
		<div>
			<form action="#" method="post">
				Naročnina:
				<input type="radio" name="narocnina" value="vsi" <%=narocninaChecked[0] %>>Vsi
				<input type="radio" name="narocnina" value="brez" <%=narocninaChecked[1] %>>Brez naročnine
				<input type="radio" name="narocnina" value="z" <%=narocninaChecked[2] %>>Z naročnino
				<br><br>
				Operaterji:
				<%				
				for (int i = 0; i < operaterji.length; i++){
					%>
					<input type="checkbox" name="operater" value="<%=operaterji[i]%>" <%=operaterjiChecked[i] %>><%=operaterji[i]%>
					<%	
				}
				%>
				<br><br>
				Prikaz:
				<input type="radio" name="prikaz" value="skrceno" <%=prikazChecked[0] %>>Skrčeno
				<input type="radio" name="prikaz" value="vse" <%=prikazChecked[1] %>>Vse
				&nbsp;
				<input type="submit" value="Potrdi">
			</form>
			<br>
		</div>
	</div>
	<div class="stolpec_levo">
		<p>
			Najbolj primerne storitve po pregledu čez celotno obdobje.
		</p>
		<%
		if(storitve!=null){
			%>
			<ol class="lista lista_no_style">
			<%=AnalizaProces.prikazSeznamaStoritev(storitve, idStoritveUporabnika, operaterjiIzbrani, narocnina, prikaz) %>
			</ol>
			<%
		}
		else{
			%>
			<small>Premalo podatkov za rangiranje storitev.</small>
			<%
		}
		%>
	</div>
	<div class="stolpec_desno">
		<p>
			Najbolj primerne storitve od največjega odstopanja (<b><%=oRezultati.vrniVrednost("datumOdstopanja") %></b>) naprej.
		</p>
		<%
		if(storitveOpti!=null){
			%>
			<ol class="lista lista_no_style">
			<%=AnalizaProces.prikazSeznamaStoritev(storitveOpti, idStoritveUporabnika, operaterjiIzbrani, narocnina, prikaz)%>
			</ol>
			<%
		}
		else{
			%>
			<small>V vaši porabi nismo zaznali nobenega večjega odstopanja.</small>
			<%
		}
		%>
	</div>
	<div style="clear: both;"></div>
</div>
<br><br>
<%
String operterjiNiz="";

for (int i = 0; i < operaterji.length; i++) {
	
	operterjiNiz+=operaterji[i];
	
	if(i<(operaterji.length-1)){
		operterjiNiz+=",";
	}
	
}

%>
<input type="hidden" id="storitve" value='<%=AnalizaProces.grafStoritev((String)session.getAttribute("id_uporabnika"))%>'/>
<input type="hidden" id="operaterji" value="<%=operterjiNiz%>"/>
<div id="graf_storitev" class="graf"></div>
<script type="text/javascript">

	$(function () {
		
		var kategorije=$("#operaterji").val().split(",");
		var podatki=JSON.parse($("#storitve").val());
		
		$('#graf_storitev').highcharts({

		    chart: {
		        type: 'bubble',
		        backgroundColor:'transparent',
		    },
		    plotOptions: {
	            bubble:{
	            	minSize:1,
	                maxSize:20,
	            },
	        },
	        tooltip: {
	        	formatter: function () {
                    return "<b>"+this.point.name+"</b><br>Narocnina: "+this.point.x+" €";
                }
	        },
		    title: {
	            text: 'Grafični prikaz storitev glede na mesečno naročnino'
	        },
	        legend: {
	            enabled: false
	        },
	        yAxis: {
	            startOnTick: false,
	            endOnTick: false,
	            categories: kategorije,
		        title: {
	                text: null
	            }
	        },
		    series: [{
		        data: podatki,
		    }]
		});
		
	});
	
</script>

<h1>Tujina</h1>
<%
Tujina t=new Tujina((String)session.getAttribute("id_uporabnika"));
t.obdelava();
Rezultat tRezultati=t.vrniRezultate();
t.koncaj();
%>
<p>
	Zaznali smo <b><%=tRezultati.vrniVrednost("stOdstopanj")%></b> odstopanj. Število različnih držav je <b><%=tRezultati.vrniVrednost("stDrzav")%></b>. Skupna cena porabljenih enot je <b><%=tRezultati.vrniVrednost("skupnaCena")%></b> €. 
	Povrpečna mesečna cena enot je <b><%=tRezultati.vrniVrednost("mesecnaCena")%></b> €.
	<% if(tRezultati.vrniVrednost("nasvet")!=null){ %>
		Glede na vašo porabo v tujini vam svetujemo da <b><%=tRezultati.vrniVrednost("nasvet")%></b>.
	<% }else{ %>
		Vaša poraba v tujini je zelo majhna. Zato ni potrebe po dodatnih ukrepih.
	<% } %>
</p>
<h1>Mesečna statistika</h1>
<%
MesecnaStatistika ms=new MesecnaStatistika(idUporabnika);
ms.obdelava();
Rezultat msRezultati=ms.vrniRezultate();
ms.koncaj();

String[] mesecnaStatistika=msRezultati.vrniVrednosti("mesecnaStatistika");
%>
<p>
	Mesečna statistika se izračuna tako, da se vzame prvo polovico in se izračuna klasifikator. Nato se s klasifikatorjem simulira drugo polovico. 
	Simulirane podatke se nato primerja z dejanskimi.
</p>
<% if(mesecnaStatistika!=null && mesecnaStatistika.length>0){ %>
	Rezultat simulacije je:
	<ul class="lista">
		<%
		for (int i = 0; i < mesecnaStatistika.length; i++){
		%>
			<li><b><%=mesecnaStatistika[i]%></b></li>
		<%	
		}
		%>
	</ul>
	<br>
	<p>
		Svetujemo vam izbiro storitve, katera ima več zgoraj omenjenih enot.
	</p>
<% }else{ %>
	<p>
		Vaša mesečna poraba v drugi polovici meseca, ne odstopa od povprečja.
	</p>
<% } %>
<h1>Obračunski intervali</h1>
<p>
	Obračunski interval z najmanjšo ceno je najbolj primeren. Z zeleno je označen vaš trenutni interval.
	
</p>
<div id="graf_obracunski_intervali" class="graf"></div>
<%
ObracunskiInterval oi=new ObracunskiInterval(idUporabnika);
oi.obdelava();
Rezultat oiRezultati=oi.vrniRezultate();
oi.koncaj();

String imena=oiRezultati.vrniVrednost("imenaIntervalov");
String vrednosti=oiRezultati.vrniVrednost("vrednostiIntervalov");

String intervalUporabnika=oiRezultati.vrniVrednost("intervalUporabnika");
%>
<input type="hidden" id="data_names" value="<%=imena%>"/>
<input type="hidden" id="data_val" value="<%=vrednosti%>"/>
<input type="hidden" id="user_interval" value="<%=intervalUporabnika%>"/>
<script type="text/javascript">

	$(function () {
		
		var data_names=($("#data_names").val()).split(";");
		data_names.splice(data_names.length-1,1);
		var data_val_str=($("#data_val").val()).split(";");
		var user_interval_str=$("#user_interval").val();
		
		var data_val=new Array();
		for (var i = 0; i < data_val_str.length-1; i++) {
			
			if(data_names[i]==user_interval_str){
				data_val[i]={y: parseInt(data_val_str[i]), color: "#30DA60"};
			}
			else{
				data_val[i]=parseInt(data_val_str[i]);
			}
		}
		
	    $('#graf_obracunski_intervali').highcharts({
	        chart: {
	            type: 'column',
	            backgroundColor:'transparent'
	        },
	        title: {
	            text: 'Obračunski intervali'
	        },
	        legend: {
	            enabled: false
	        },
	        xAxis: {
	            categories: data_names
	        },
	        yAxis: {
	            title: {
	                text: 'Vrednost obračunskega intervala'
	            }
	        },
	        tooltip: {
	            enabled: false,
	        },
	        series: [{
	            name: 'Vrednost intervala',
	            data: data_val,
	            dataLabels: {
                    enabled: true,
                }
	        }]

	    });
	});
</script>
