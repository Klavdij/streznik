<%@ page import="procesi.PodatkiProces" %>
<%

	String output="";

	String kategorija=request.getParameter("kategorija");
	String tip=request.getParameter("tip");
	String idUpo=request.getParameter("uporabnik");
	String odhodniParm=request.getParameter("odhodni");
	
	if(kategorija==null && tip==null && idUpo==null){
		output="JSON ni zgrajen ker niso nastavjeni parametri!";
	}
	else{
		int odhodni=-1;
		if(odhodniParm!=null){
			try{
				odhodni=Integer.parseInt(odhodniParm);
			} catch(Exception e){}
		}
		char t=tip.charAt(0);
		if(kategorija!=null && kategorija.equals("statistika")){
			output=PodatkiProces.podatkiJSON(idUpo,t,true,odhodni);
			
		}
		else if(kategorija!=null && kategorija.equals("primerjava")){
			output=PodatkiProces.podatkiJSON(idUpo,t,false,odhodni);
		}
	}

%>
<%=output%>