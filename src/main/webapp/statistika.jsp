﻿<% String idUpo=(String)session.getAttribute("id_uporabnika"); %>
<input type="hidden" id="uporabnik" value="<%= (String)session.getAttribute("id_uporabnika")%>"/>
<%
	String prikazaniPodatki="-1";
	String p=request.getParameter("prikaz");
	if(p!=null){
		prikazaniPodatki=p;	
	}

%>
<input type="hidden" id="prikazi" value="<%=prikazaniPodatki%>"/>
<h1>Pregled porabe skozi mesece</h1>
<p>
Prikaz:
<input type="button" id="gumb1" onclick="prikazPodatkov()" value="Vsi">
<input type="button" id="gumb2" onclick="prikazPodatkov(1)" value="Odhodni">
<input type="button" id="gumb3" onclick="prikazPodatkov(0)" value="Dohodni">
</p>
<script type="text/javascript">

var uporabnik=$("#uporabnik").val();

var prikaz=$("#prikazi").val();

if(prikaz=="1"){
	$("#gumb2").attr("disabled", true);
}
else if (prikaz=="0"){
	$("#gumb3").attr("disabled", true);
}
else{
	$("#gumb1").attr("disabled", true);
}

function prikazPodatkov(prikazi){
	if(prikazi==null){
		window.location.href = "index.jsp?page=statistika";
	}
	else{
		window.location.href = "index.jsp?page=statistika&prikaz="+prikazi;
	}
}

</script>
<h2>SMS</h2>
<div id="graf_sms" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="sms" value="day">Dnevih
					<input type="radio" name="sms" value="week">Tednih 
					<input type="radio" name="sms" value="month" checked>Mesecih</div>
<script type="text/javascript">

	$(function () { 
		var data_link="podatki.jsp?kategorija=statistika&tip=s&uporabnik="+uporabnik+"&odhodni="+prikaz;
		
		$.getJSON(data_link, function (data) {
			
			$('#graf_sms').highcharts('StockChart', {
				title: {
					text: 'Poraba sms-ov'
			    },
			    chart: {
		            backgroundColor:'transparent'
		        },
			    series: [{
			        name: 'Porabljenih sms-ov',
			        data: data,
			        marker : {
						enabled : true,
						radius : 3
					},
			        dataGrouping: {
			            approximation: "sum",
			            enabled: true,
			            forced: true,
			            units: [['month',[1]]]
			        }
			    }]
			});
		});
	});
</script>
<h2>MMS</h2>
<div id="graf_mms" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="mms" value="day">Dnevih
					<input type="radio" name="mms" value="week">Tednih 
					<input type="radio" name="mms" value="month" checked>Mesecih</div>
<script type="text/javascript">

	$(function () { 
		var data_link="podatki.jsp?kategorija=statistika&tip=m&uporabnik="+uporabnik+"&odhodni="+prikaz;
		
		$.getJSON(data_link, function (data) {
			$('#graf_mms').highcharts('StockChart', {
				title: {
					text: 'Poraba mms-ov'
			    },
			    chart: {
		            backgroundColor:'transparent'
		        },
			    series: [{
			        name: 'Porabljenih mms-ov',
			        data: data,
			        marker : {
						enabled : true,
						radius : 3
					},
			        dataGrouping: {
			            approximation: "sum",
			            enabled: true,
			            forced: true,
			            units: [['month',[1]]]
			        }
			    }]
			});
		});
	});
</script>
<h2>Klici</h2>
<div id="graf_klici" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="klici" value="day">Dnevih
					<input type="radio" name="klici" value="week">Tednih 
					<input type="radio" name="klici" value="month" checked>Mesecih</div>
<script type="text/javascript">

	$(function () { 
		var data_link="podatki.jsp?kategorija=statistika&tip=k&uporabnik="+uporabnik+"&odhodni="+prikaz;
		
		$.getJSON(data_link, function (data) {
			$('#graf_klici').highcharts('StockChart', {
				title: {
					text: 'Porabljene minute klicev'
			    },
			    chart: {
		            backgroundColor:'transparent'
		        },
			    series: [{
			        name: 'Porabljene minute',
			        data: data,
			        marker : {
						enabled : true,
						radius : 3
					},
			        dataGrouping: {
			            approximation: "sum",
			            enabled: true,
			            forced: true,
			            units: [['month',[1]]]
			        }
			    }]
			});
		});
	});
</script>
<h2>Internet</h2>
<div id="graf_net" class="graf"></div>
<div>Grupiraj po: <input type="radio" name="net" value="day">Dnevih
					<input type="radio" name="net" value="week">Tednih 
					<input type="radio" name="net" value="month" checked>Mesecih</div>
<script type="text/javascript">

	$(function () {
		var data_link="podatki.jsp?kategorija=statistika&tip=n&uporabnik="+uporabnik;
		
		$.getJSON(data_link, function (data) {
			$('#graf_net').highcharts('StockChart', {
				title: {
					text: 'Porabljeni megabajti'
			    },
			    chart: {
		            backgroundColor:'transparent'
		        },
			    series: [{
			        name: 'Porabljeni mB',
			        data: data,
			        marker : {
						enabled : true,
						radius : 3
					},
			        dataGrouping: {
			            approximation: "sum",
			            enabled: true,
			            forced: true,
			            units: [['month',[1]]]
			        }
			    }]
			});
		});
	});
	

	$.each(['sms', 'mms', 'klici', 'net'], function (i, type) {
		
	    $('input[name='+type+']').click(function () {
	    	
	    	var chart=$('#graf_'+type).highcharts();
	    	
	    	var value=$('input[name='+type+']:checked').val();
	    	
	        chart.series[0].update({
	        	dataGrouping: {
		            units: [[value,[1]]]
		        }
	        });
	    });
	});

</script>
