﻿<%@ page import="procesi.IndexProces" %>
<%@ page import="com.mongodb.BasicDBObject" %>
<%

String idStoritve="";

if(request.getParameter("id")!=null){
	idStoritve=request.getParameter("id");
}

BasicDBObject storitev=IndexProces.vrniStoritevIzId(idStoritve);

%>
<h1>Pregled storitve</h1>
<div class="opis_storitve">
	<div>Ime: <b><%=storitev.get("ime") %></b></div>
	<div>Operater: <b><%=storitev.get("operater") %></b></div>
	<div>Mesečna naročnina: <b><%=storitev.get("mesecna_narocnina") %> €</b></div>
	<div>Obračunksi interval za klice je <b><%=storitev.get("obracunski_interval_klic") %>/<%=storitev.get("obracunski_interval_enota") %></b> in internet <b><%=storitev.get("obracunski_interval_net") %> kB</b>.</div>
	<br>
	<div>
		<div class="stolpec_levo">
			<div>Zakupljena količina</div>
			<ul class="lista_no_style">
				<li><div>SMS/MMS: <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_sms_mms")) %></b></div></li>
				<li><div>Klici (domače): <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_klici_domace")) %></b></div></li>
				<li><div>Klici (tuje/stac): <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_klici_tuje_stac")) %></b></div></li>
				<li><div>Internet: <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_net")) %></b></div></li>
				<li><div>Skupna: <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_skupna")) %></b></div></li>
			</ul>
		</div>
		<div class="stolpec_desno">
			<div>Cena na enoto</div>
			<ul class="lista_no_style">
				<li><div>SMS/MMS: <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_sms_mms")) %> €</b></div></li>
				<li><div>Klici (domače): <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_klic_domace")) %> €</b></div></li>
				<li><div>Klici (tuje): <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_klic_tuje")) %> €</b></div></li>
				<li><div>Klici (stac): <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_klic_stac")) %> €</b></div></li>
				<li><div>Internet: <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_net")) %> €</b></div></li>
			</ul>
		</div>
		<div style="clear: both;"></div>
	</div>
</div>
<span class="opomba">Enota za klic je minuta, za internet kB.</span>
<br><br>
<div>
	<a class="colored" href="?page=analiza">Nazaj</a>
</div>