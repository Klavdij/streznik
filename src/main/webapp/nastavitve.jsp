﻿<%@page import="com.mongodb.BasicDBList,com.mongodb.BasicDBObject"%>
<%@page import="java.util.ArrayList,java.util.Collection"%>
<%@page import="procesi.NastavitveProces,java.util.Enumeration,java.util.Calendar" %>
<%
	String emso=NastavitveProces.emsoUporabnika((String)session.getAttribute("id_uporabnika"));
	String obvestilo="";
	
	if(request.getParameter("generiraj")!=null){
		NastavitveProces.generirajPodatke(request.getParameter("nastavitve_generiranja"));
		obvestilo="Podatki generirani";
	}
	
	if(request.getParameter("odstrani_storitev")!=null){
		String id=request.getParameter("odstrani_storitev");
		if(NastavitveProces.izbrisiStoritev(id)){
			obvestilo = "Storitev uspešno izbrisana.";
		}
		else{
			obvestilo = "Napaka pri brisanju storitve.";
		}
	}
	
	if(request.getParameter("dodaj_storitev")!=null){
		Enumeration<String> paramNames = request.getParameterNames();
		String[] parameters;
		ArrayList<String[]> requestList=new ArrayList<String[]>();
		String id=null;
		
		if(!request.getParameter("dodaj_storitev").equals("true")){
			id=request.getParameter("dodaj_storitev");
		}
		
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			if(!paramName.equals("page") && !paramName.equals("dodaj_storitev")){
				String paramValue = request.getParameter(paramName);
				parameters=new String[2];
				parameters[0]=paramName;
				parameters[1]=paramValue;
				requestList.add(parameters);
			}
		}
		
		if(NastavitveProces.shraniStoritev(requestList,id)){
			obvestilo = "Storitev uspešno shranjena.";
		}
		else{
			obvestilo = "Napaka pri shranjevanju storitve.";
		}
	}
%>
<div class="obvestilo"><%=obvestilo %></div>
<h1>Generiraj podatke</h1>
<p>
	Generira podatke za trenutno izbranega uporabnika.<br><br>
	Opis:<br>
	UTEZI_MESECI - Razpored klicev skozi posamezne mesece. Razdelimo jih po procentih (od 0 do 1). Skupen seštevek je 1.<br>
	UTEZI_MESEC - Procent klicev v prvi polovici meseca.<br>
	UTEZI_URE - Procent klicev skozi dan.<br>
	UTEZI_OMREZJA - Procent klicev v omrežja (mobilna, stacionarna, posebna, voIP).<br>
	UTEZ_LASTNO_OMREZJE - Procent klicev v lastno omrežje.<br>
	X_ST - Stevilo klicev, SMS, MMS, Podatkov.<br>
	KLICI_TRAJANJE_X - Obseg trajanje klica v sekundah.<br>
	X_KOLICINA_KB_OBSEG_X - Obseg prenesenih kB.<br>
</p>
<form action="index.jsp?page=nastavitve" method="post">
<textarea name="nastavitve_generiranja" rows="26">
# Splosno
EMSO=<%=emso %>
LETO=<%=Calendar.getInstance().get(Calendar.YEAR) %>
LOKACIJA_X=46.057181
LOKACIJA_Y=14.507354
# Utezi
UTEZI_MESECI=0.08,0.08,0.08,0.08,0.08,0.1,0.1,0.08,0.08,0.08,0.08,0.08
UTEZI_MESEC=0.5
UTEZI_URE=0.15,0.35,0.35,0.15
UTEZI_OMREZJA=0.8,0.1,0.05,0.05
UTEZ_LASTNO_OMREZJE=0.5
# Klici
KLICI_ST=24000
KLICI_TRAJANJE_OD=20
KLICI_TRAJANJE_DO=200
# SMS
SMS_ST=24000
# MMS
MMS_ST=2400
# NET
NET_ST=1200
NET_KOLICINA_KB_OBSEG_OD=100
NET_KOLICINA_KB_OBSEG_DO=4096
</textarea>
<br><br>
<input type="hidden" name="generiraj" value="true">
<input type="submit" value="Generiraj">
</form><br>

<h1>Urejanje mobilnih storitev</h1>
<script>
	function urediStoritev(id){
		var jsonPodatki=$("#"+id).val();
		var jsonObjekt=$.parseJSON(jsonPodatki);
		
	    $.each(jsonObjekt, function(index, element) {
	       	if(index!="_id"){
	       		$('[name="'+index+'"]').val(element);
	       	}
	    });
	    $('[name="dodaj_storitev"]').val(id);
	}
	
	function brisiStoritev(id){
		if(confirm("Ali želiš izbrisati storitev?")){
			$("#odstrani_storitev").val(id);
			$("#form_brisi").submit();
		}
	}
</script>
<p>
	Forma za dodajanje novih mobilnih storitev storitev. <br>
	Ceno podajamo z piko (ne vejico). Pri količini -1 označuje neskončno.
</p>
<form id="form_brisi" action="index.jsp?page=nastavitve" method="post">
<ol class="list_storitev">
<%
	BasicDBList listStoritev=NastavitveProces.listStoritev();

	for (int i = 0; i < listStoritev.size(); i++) {
		BasicDBObject o=(BasicDBObject) listStoritev.get(i);
		
		%>
		<li>
			<a href="javascript:urediStoritev('<%=o.getString("_id") %>');"><%=o.getString("operater") %> - <%=o.getString("ime") %></a>
			<input type='hidden' id='<%=o.getString("_id") %>' value='<%=o.toString()%>'>
			<input type="button" value="Izbriši" onclick="brisiStoritev('<%=o.getString("_id") %>')">
		</li>
		<%
	}
%>
</ol>
<input id="odstrani_storitev" type="hidden" value="" name="odstrani_storitev">
</form>
<form action="index.jsp?page=nastavitve" method="post">
	<table>
	<tr><td align="right">Ime:</td><td align="left"><input type="text" name="ime" size="20"></td></tr>
	<tr><td align="right">Operater:</td><td align="left"><input type="text" name="operater" size="20"></td></tr>
	<tr><td align="right">Mesečna naročnina:</td><td align="left"><input type="text" name="mesecna_narocnina" size="4"></td></tr>
	<tr><td align="right">Obračunski interval za klic:</td><td align="left"><input type="text" name="obracunski_interval_klic" size="4"></td></tr>
	<tr><td align="right">Obračunski interval enot:</td><td align="left"><input type="text" name="obracunski_interval_enota" size="4"></td></tr>
	<tr><td align="right">Cena na enoto za klic (domače):</td><td align="left"><input type="text" name="cena_na_enoto_klic_domace" size="4"></td></tr>
	<tr><td align="right">Cena na enoto za klic (tuje):</td><td align="left"><input type="text" name="cena_na_enoto_klic_tuje" size="4"></td></tr>
	<tr><td align="right">Cena na enoto za klic (stacionarno):</td><td align="left"><input type="text" name="cena_na_enoto_klic_stac" size="4"></td></tr>
	<tr><td align="right">Zakupljena količina za klice (domače):</td><td align="left"><input type="text" name="zakupljena_kolicina_klici_domace" size="4"></td></tr>
	<tr><td align="right">Zakupljena količina za klice (tuje in stac):</td><td align="left"><input type="text" name="zakupljena_kolicina_klici_tuje_stac" size="4"></td></tr>
	<tr><td align="right">Cena na enoto za SMS/MMS:</td><td align="left"><input type="text" name="cena_na_enoto_sms_mms" size="4"></td></tr>
	<tr><td align="right">Zakupljena količina za SMS/MMS:</td><td align="left"><input type="text" name="zakupljena_kolicina_sms_mms" size="4"></td></tr>
	<tr><td align="right">Cena na enoto za NET:</td><td align="left"><input type="text" name="cena_na_enoto_net" size="4"></td></tr>
	<tr><td align="right">Obračunski interval za NET:</td><td align="left"><input type="text" name="obracunski_interval_net" size="4"></td></tr>
	<tr><td align="right">Zakupljena količina za NET:</td><td align="left"><input type="text" name="zakupljena_kolicina_net" size="4"></td></tr>
	<tr><td align="right">Skupna zakupljena količina:</td><td align="left"><input type="text" name="zakupljena_kolicina_skupna" size="4"></td></tr>
	<tr><td><input type="hidden" name="dodaj_storitev" value="true"></td>
	<td align="right"><input type="submit" value="Dodaj"></td></tr>
	</table>
</form>
<h1>Uporabniki</h1>
<input type="button" value="Zamenjaj uporabnika" onclick="window.location.href='index.jsp?action=zamenjaj_uporabnika'">