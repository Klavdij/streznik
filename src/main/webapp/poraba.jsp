﻿<%@ page import="procesi.PorabaProces" %>
<input type="hidden" id="data" value="<%= PorabaProces.porabljenaSredstva((String)session.getAttribute("id_uporabnika"))%>"/>
<h1>Poraba za mesec <%=PorabaProces.vrniTrenutniMesec() %></h1>
<div id="graf_poraba" class="graf"></div>
<div>Prikaz: <input type="radio" name="prikaz" value="0" checked>Vsi
					<input type="radio" name="prikaz" value="1">Odhodni 
					<input type="radio" name="prikaz" value="2">Dohodni
					&nbsp;<span class="opomba">( Internetni prenos se vedno prikazuje v celoti. )</span>
					</div>
<script type="text/javascript">

	$(function () {
	
		var dataArray=($("#data").val()).split(";");
		d= new Array();
		
		for (var i = 0; i < dataArray.length; i++) {
			var data=dataArray[i].split(",");
			
			d[i]=new Array();
			d[i][0]=parseInt(data[0]);
			d[i][1]=parseInt(data[1]);
			d[i][2]=parseInt(data[2]);
			d[i][3]=parseInt(data[3]);
		}
		
	    $('#graf_poraba').highcharts({
	        chart: {
	            type: 'bar',
	            backgroundColor:'transparent'
	        },
	        title: {
	            text: 'Poraba sredstev'
	        },
	        legend: {
	            enabled: false
	        },
	        xAxis: {
	            categories: ['SMS/MMS', 'Klici domače', 'Klici tuje', 'Prenos podatkov']
	        },
	        yAxis: {
	            title: {
	                text: 'Porabljena sredstva za trenutni mesec'
	            }
	        },
	        series: [{
	            name: 'Količina sredstev',
	            data: d[0],
	            dataLabels: {
                    enabled: true,
                }
	        }]

	    });
	});
	
	$('input[name=prikaz]').click(function () {
    	
    	var chart=$('#graf_poraba').highcharts();
    	
    	var value=$('input[name=prikaz]:checked').val();
    	
        chart.series[0].update({
	    	data: d[value]
        });
    });
</script>
<h1>Procentna poraba po omrežjih</h1>
<input type="hidden" id="omrezja" value='<%= PorabaProces.procentKlicevVOmrezja((String)session.getAttribute("id_uporabnika"))%>'/>
<div id="graf_procenti_omrezja" class="graf"></div>
<span class="opomba">Podatki zbrani skozi celotno uporabo sistema.</span>
<script type="text/javascript">
	$(function () {
		
		var podatki=JSON.parse($("#omrezja").val());
		
		$('#graf_procenti_omrezja').highcharts({
	        chart: {
	        	type: 'column',
	            backgroundColor:'transparent'
	        },
	        title: {
	            text: 'Procentna poraba čez celotno obdobje'
	        },
	        tooltip: {
	    	    pointFormat: '{series.name}: <b>{point.y:.2f}%</b>'
	        },
	        legend: {
	        	enabled: false,
	        },
	        xAxis: {
	            type: "category"
	        },
	        plotOptions: {
	            column: {
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y:.1f}',
	                }
	            }
	        },
	        series: [podatki]
	    });
	});
</script>
<input type="hidden" id="omrezja_leta" value='<%=PorabaProces.procentKlicevVOmrezjaPoLetih((String)session.getAttribute("id_uporabnika"))%>'/>
<div id="graf_procenti_omrezja_leta" class="graf"></div>
<script type="text/javascript">
	$(function () {
		
		var podatki=JSON.parse($("#omrezja_leta").val());
		
		$('#graf_procenti_omrezja_leta').highcharts({
	        chart: {
	        	type: 'column',
	            backgroundColor:'transparent'
	        },
	        title: {
	            text: 'Procentna poraba po letih'
	        },
	        tooltip: {
	    	    pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
	        },
	        xAxis: {
	            type: "category"
	        },
	        plotOptions: {
	            column: {
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.y:.1f}',
	                }
	            }
	        },
	        series: podatki
	    });
	});
</script>