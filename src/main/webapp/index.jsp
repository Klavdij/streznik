<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="procesi.IndexProces" %>
<%@ page import="com.mongodb.BasicDBObject" %>
<%@page import="rangiranje.rezultati.Rezultat"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1">
	
	<title>Nadzor porabe</title>
	<script type="text/javascript"  src="js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript"  src="js/highstock.js"></script>
	<script type="text/javascript"  src="js/highcharts-more.js"></script>
	<link href="css/slogi.css" rel="stylesheet" type="text/css" >
	<!--[if IE]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
</head>
<body>
	<header>
	<a href="#" id="logo">Nadzor porabe</a>

		<%
		String napakaPrijave="";
		
		//Odstranimo parameter iz sesion-na
		String a=request.getParameter("action");
		if(a!=null && a.equals("zamenjaj_uporabnika")){
			session.removeAttribute("id_uporabnika");
		}

		// Nastavimo session na id uporabnika
		if(request.getParameter("izbranUporabnik")!=null){
			String idUporabnika=request.getParameter("uporabnik");
			String geslo=request.getParameter("geslo");
			
			if(IndexProces.ustreznoGeslo(idUporabnika, geslo)){
				session.setAttribute("id_uporabnika", idUporabnika);
			}
			else{
				napakaPrijave="<div class='prijava' style='color: #FF0000;'>Napačno geslo!</div>";
			}
			
		}
		else if(request.getParameter("u")!=null){
			session.setAttribute("id_uporabnika", request.getParameter("u"));
		}
		
			
		String p=null;
		String[] izbran=new String[6];
			
		if(session.getAttribute("id_uporabnika")!=null){
		
			int index=0;
			
			if (request.getParameter("page") != null) {
				p = request.getParameter("page");

				if(p.equals("poraba")){
					index=1;
				}
				if(p.equals("analiza")){
					index=2;
				}
				else if(p.equals("storitev")){
					index=2;
				}
				else if(p.equals("statistika")){
					index=3;
				}
				else if(p.equals("primerjava")){
					index=4;
				}
				else if(p.equals("nastavitve")){
					index=5;
				}
			}
		
			izbran[index]="current";
			%>
			<nav>
				<a href="#" id="menu-icon"></a>
				<ul>
					<li><a class="<%=izbran[0]%>" href="index.jsp">Domov</a></li>
					<li><a class="<%=izbran[1]%>" href="index.jsp?page=poraba">Poraba</a></li>
					<li><a class="<%=izbran[2]%>" href="index.jsp?page=analiza">Analiza</a></li>
					<li><a class="<%=izbran[3]%>" href="index.jsp?page=statistika">Statistika</a></li>
					<li><a class="<%=izbran[4]%>" href="index.jsp?page=primerjava">Primerjava</a></li>
					<li><a class="<%=izbran[5]%>" href="index.jsp?page=nastavitve">Nastavitve</a></li>
				</ul>
			</nav>
			<%
		}
		%>
		</header>
		<section>
		<%
		if (p != null) {
			p = request.getParameter("page");
			p = p + ".jsp";
			%>
			<jsp:include page="<%=p%>"/>
			<%
		}
		else if(session.getAttribute("id_uporabnika")==null){
			String [][]uporabniki=IndexProces.vrniUporabnike();
			%>
			<h2>Izberi uporabnika:</h2>
			<form action="index.jsp" method="post">
			<div class="prijava">
				Uporabnik:<br>
				<select name="uporabnik">
				<%
				for(int i=0;i<uporabniki.length;i++){
					%>
					<option value="<%=uporabniki[i][1]%>"><%=uporabniki[i][0]%></option>
					<%
				}
				%>
				</select>
			</div>
			<div class="prijava">
				Geslo:<br>
				<input type="password"  name="geslo">
			</div>
			<%=napakaPrijave %>
			<input type="hidden" name="izbranUporabnik" value="true">
			<input type="submit" value="Izberi">
			</form>
			<%
		}
		else{
			String idUporabnika=(String)session.getAttribute("id_uporabnika");
			BasicDBObject storitev=IndexProces.vrniStoritev(idUporabnika);
			
			Rezultat cenaPorabe=IndexProces.vrniCenePorabe(idUporabnika);
		
			%>
			<h1>Informacijski sistem</h1>
			<h2>Opis</h2>
			<p>
				Pregled trenutne porabe, analiza porabe, statistika in primerjava porabe z ostalimi uporabniki.
			</p>
			<h2>Trenutna cena porabe</h2>
			<p>
				<small>Mesečna naročnina:</small> <b><%=cenaPorabe.vrniVrednost("mesecnaNarocnina") %></b> €
				+ <small>Cena dodatnih enot:</small> <b><%=cenaPorabe.vrniVrednost("cenaEnot") %></b> €
				+ <small>Tujina:</small> <b><%=cenaPorabe.vrniVrednost("cenaTujina") %></b> €
				= <small>Skupaj:</small> <b><%=cenaPorabe.vrniVrednost("cenaSkupna") %></b> €
			</p>
			<h2>Storitev</h2>
			<p>
			Trenutno uporabljate naslednjo stritev:
			</p>
			<div class="opis_storitve">
				<div>Ime: <b><%=storitev.get("ime") %></b></div>
				<div>Operater: <b><%=storitev.get("operater") %></b></div>
				<div>Mesečna naročnina: <b><%=storitev.get("mesecna_narocnina") %> €</b></div>
				<div>Vezava od <b><%=IndexProces.izpisiDatum(storitev.getDate("datum_od")) %></b> do <b><%=IndexProces.izpisiDatum(storitev.getDate("datum_do")) %></b>.</div>
				<div>Obračunksi interval za klice je <b><%=storitev.get("obracunski_interval_klic") %>/<%=storitev.get("obracunski_interval_enota") %></b> in internet <b><%=storitev.get("obracunski_interval_net") %> kB</b>.</div>
				<br>
				<div>
					<div class="stolpec_levo">
						<div>Zakupljena količina</div>
						<ul class="lista_no_style">
							<li><div>SMS/MMS: <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_sms_mms")) %></b></div></li>
							<li><div>Klici (domače): <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_klici_domace")) %></b></div></li>
							<li><div>Klici (tuje/stac): <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_klici_tuje_stac")) %></b></div></li>
							<li><div>Internet: <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_net")) %></b></div></li>
							<li><div>Skupna: <b><%=IndexProces.izpisEnot(storitev.get("zakupljena_kolicina_skupna")) %></b></div></li>
						</ul>
					</div>
					<div class="stolpec_desno">
						<div>Cena na enoto</div>
						<ul class="lista_no_style">
							<li><div>SMS/MMS: <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_sms_mms")) %> €</b></div></li>
							<li><div>Klici (domače): <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_klic_domace")) %> €</b></div></li>
							<li><div>Klici (tuje): <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_klic_tuje")) %> €</b></div></li>
							<li><div>Klici (stac): <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_klic_stac")) %> €</b></div></li>
							<li><div>Internet: <b><%=IndexProces.izpisCene(storitev.get("cena_na_enoto_net")) %> €</b></div></li>
						</ul>
					</div>
					<div style="clear: both;"></div>
				</div>
			</div>
			<span class="opomba">Enota za klic je minuta, za internet kB.</span>
			<%
		}
	%>
	</section>
</body>
</html>