package entitete;

import java.util.HashMap;

import org.bson.types.ObjectId;

import orodja.Baza;

public class SeznamMesecniPodatkiTujina {
	
	private int size;
	
	private ObjectId idUporabnika;
	
	HashMap<String, HashMap<String, MesecniPodatkiTujina>> seznam;

	public SeznamMesecniPodatkiTujina(ObjectId idUporabnika) {
		
		size=0;
		
		this.idUporabnika=idUporabnika;
		
		seznam=new HashMap<String, HashMap<String,MesecniPodatkiTujina>>();
	}
	
	public int getSize() {
		return size;
	}

	public MesecniPodatkiTujina vrniMesecniPodatekZaTujino(int leto, int mesec, String drzava){
		
		String idLetoMesec=leto+"-"+mesec;
		
		MesecniPodatkiTujina mpt;
		
		HashMap<String, MesecniPodatkiTujina> subSeznam;
		
		subSeznam=seznam.get(idLetoMesec);
		
		if(subSeznam==null){
			subSeznam=new HashMap<String, MesecniPodatkiTujina>();
			
			mpt=new MesecniPodatkiTujina(idUporabnika, leto, mesec, drzava);
			
			subSeznam.put(drzava, mpt);
			seznam.put(idLetoMesec, subSeznam);
			
			size++;
		}
		else{
			
			mpt=subSeznam.get(drzava);
			
			if(mpt==null){
				
				mpt=new MesecniPodatkiTujina(idUporabnika, leto, mesec, drzava);
				
				subSeznam.put(drzava, mpt);
				
				size++;
			}
			
		}
		
		return mpt;
	}

	public void shraniVBazo(Baza baza){
		
		MesecniPodatkiTujina mpt;
		
		HashMap<String, MesecniPodatkiTujina> subSeznam;
		
		for ( String keySeznam : seznam.keySet() ) {
			
			subSeznam=seznam.get(keySeznam);
			
			for ( String keySubSeznam : subSeznam.keySet() ) {
				mpt=subSeznam.get(keySubSeznam);
				baza.shraniStandardnePodatke(mpt);
			}
		}		
	}
	
}
