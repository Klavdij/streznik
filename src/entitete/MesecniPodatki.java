package entitete;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;

public class MesecniPodatki implements Entiteta{
	
	public static final String ZBIRKA="mesecniPodatki";
	
	protected ObjectId id;
	protected ObjectId idUporabnika;
	protected int leto;
	protected int mesec;
	protected int kolicinaKliciDomace;
	protected int kolicinaKliciTuje;
	protected int kolicinaKliciStac;
	protected int kolicinaSmsMms;
	protected int kolicinaNet;	
	
	
	public MesecniPodatki() {
		super();
	}

	public MesecniPodatki(ObjectId idUporabnika, int leto, int mesec,
			int kolicinaKliciDomace, int kolicinaKliciTuje,int kolicinaKliciStac,
			int kolicinaSmsMms, int kolicinaNet) {
		super();
		this.id=null;
		this.idUporabnika = idUporabnika;
		this.leto = leto;
		this.mesec = mesec;
		this.kolicinaKliciDomace = kolicinaKliciDomace;
		this.kolicinaKliciTuje = kolicinaKliciTuje;
		this.kolicinaKliciStac = kolicinaKliciStac;
		this.kolicinaSmsMms = kolicinaSmsMms;
		this.kolicinaNet = kolicinaNet;
	}	
	
	public MesecniPodatki(BasicDBObject mp) {
		this.id = mp.getObjectId("_id");
		this.idUporabnika = mp.getObjectId("id_uporabnika");
		this.leto = mp.getInt("leto");
		this.mesec = mp.getInt("mesec");
		this.kolicinaKliciDomace = mp.getInt("kolicina_klici_domace");
		this.kolicinaKliciTuje = mp.getInt("kolicina_klici_tuje");
		this.kolicinaKliciStac = mp.getInt("kolicina_klici_stac");
		this.kolicinaSmsMms = mp.getInt("kolicina_sms_mms");
		this.kolicinaNet = mp.getInt("kolicina_net");
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ObjectId getIdUporabnika() {
		return idUporabnika;
	}

	public void setIdUporabnika(ObjectId idUporabnika) {
		this.idUporabnika = idUporabnika;
	}
	
	public int getLeto() {
		return leto;
	}

	public void setLeto(int leto) {
		this.leto = leto;
	}

	public int getMesec() {
		return mesec;
	}

	public void setMesec(int mesec) {
		this.mesec = mesec;
	}

	public int getKolicinaKliciDomace() {
		return kolicinaKliciDomace;
	}

	public void setKolicinaKliciDomace(int kolicinaKliciDomace) {
		this.kolicinaKliciDomace = kolicinaKliciDomace;
	}

	public int getKolicinaKliciTuje() {
		return kolicinaKliciTuje;
	}

	public void setKolicinaKliciTuje(int kolicinaKliciTuje) {
		this.kolicinaKliciTuje = kolicinaKliciTuje;
	}
	
	public int getKolicinaKliciStac() {
		return kolicinaKliciStac;
	}

	public void setKolicinaKliciStac(int kolicinaKliciStac) {
		this.kolicinaKliciStac = kolicinaKliciStac;
	}

	public int getKolicinaSmsMms() {
		return kolicinaSmsMms;
	}

	public void setKolicinaSmsMms(int kolicinaSmsMms) {
		this.kolicinaSmsMms = kolicinaSmsMms;
	}

	public int getKolicinaNet() {
		return kolicinaNet;
	}

	public void setKolicinaNet(int kolicinaNet) {
		this.kolicinaNet = kolicinaNet;
	}
	
	public BasicDBObject dobiPodatkovniObjekt(){
		BasicDBObject doc = new BasicDBObject();
		
		if(this.id!=null){
			doc.put("_id", this.id);
		}
		doc.put("id_uporabnika", this.idUporabnika);
		doc.put("leto", this.leto);
		doc.put("mesec", this.mesec);
		doc.put("kolicina_klici_domace", this.kolicinaKliciDomace);
		doc.put("kolicina_klici_tuje", this.kolicinaKliciTuje);
		doc.put("kolicina_klici_stac", this.kolicinaKliciStac);
		doc.put("kolicina_sms_mms", this.kolicinaSmsMms);
		doc.put("kolicina_net", this.kolicinaNet);
		
		return doc;
	}

	public String zbirka() {
		return MesecniPodatki.ZBIRKA;
	}
	
}
