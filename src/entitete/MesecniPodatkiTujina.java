package entitete;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;

public class MesecniPodatkiTujina implements Entiteta{
	
	public static final String ZBIRKA="mesecniPodatkiTujina";
	
	private ObjectId id;
	private ObjectId idUporabnika;
	private int leto;
	private int mesec;
	private String drzava;
	private int kolicinaKliciOdhodni;
	private int kolicinaKliciDohodni;
	private int kolicinaSmsMms;
	private int kolicinaNet;
	
	public MesecniPodatkiTujina(ObjectId idUporabnika, int leto, int mesec, String drzava) {
		super();
		this.idUporabnika = idUporabnika;
		this.leto = leto;
		this.mesec = mesec;
		this.drzava = drzava;
		this.kolicinaKliciOdhodni = 0;
		this.kolicinaKliciDohodni = 0;
		this.kolicinaSmsMms = 0;
		this.kolicinaNet = 0;
	}
	
	public MesecniPodatkiTujina(BasicDBObject mpt) {
		this.id = mpt.getObjectId("_id");
		this.idUporabnika = mpt.getObjectId("id_uporabnika");
		this.leto = mpt.getInt("leto");
		this.mesec = mpt.getInt("mesec");
		this.drzava = mpt.getString("drzava");
		this.kolicinaKliciOdhodni = mpt.getInt("kolicina_klici_odhodni");
		this.kolicinaKliciDohodni = mpt.getInt("kolicina_klici_dohodni");
		this.kolicinaSmsMms = mpt.getInt("kolicina_sms_mms");
		this.kolicinaNet = mpt.getInt("kolicina_net");
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ObjectId getIdUporabnika() {
		return idUporabnika;
	}
	
	public void setIdUporabnika(ObjectId idUporabnika) {
		this.idUporabnika = idUporabnika;
	}

	public int getLeto() {
		return leto;
	}

	public void setLeto(int leto) {
		this.leto = leto;
	}

	public int getMesec() {
		return mesec;
	}

	public void setMesec(int mesec) {
		this.mesec = mesec;
	}

	public String getDrzava() {
		return drzava;
	}

	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}

	public int getKolicinaKliciOdhodni() {
		return kolicinaKliciOdhodni;
	}
	
	public int getKolicinaKliciOdhodniMin() {
		return (int) Math.round(this.kolicinaKliciOdhodni/60.0);
	}

	public void setKolicinaKliciOdhodni(int kolicinaKliciOdhodni) {
		this.kolicinaKliciOdhodni = kolicinaKliciOdhodni;
	}
	
	public void addKolicinaKliciOdhodni(int kolicinaKliciOdhodni) {
		this.kolicinaKliciOdhodni += kolicinaKliciOdhodni;
	}

	public int getKolicinaKliciDohodni() {
		return kolicinaKliciDohodni;
	}
	
	public int getKolicinaKliciDohodniMin() {
		return (int) Math.round(this.kolicinaKliciDohodni/60.0);
	}

	public void setKolicinaKliciDohodni(int kolicinaKliciDohodni) {
		this.kolicinaKliciDohodni = kolicinaKliciDohodni;
	}
	
	public void addKolicinaKliciDohodni(int kolicinaKliciDohodni) {
		this.kolicinaKliciDohodni += kolicinaKliciDohodni;
	}

	public int getKolicinaSmsMms() {
		return kolicinaSmsMms;
	}

	public void setKolicinaSmsMms(int kolicinaSmsMms) {
		this.kolicinaSmsMms = kolicinaSmsMms;
	}
	
	public void addKolicinaSmsMms() {
		this.kolicinaSmsMms++;
	}

	public int getKolicinaNet() {
		return kolicinaNet;
	}
	
	public int getKolicinaNetMB() {
		return (int) Math.round(this.kolicinaNet/1024.0);
	}

	public void setKolicinaNet(int kolicinaNet) {
		this.kolicinaNet = kolicinaNet;
	}
	
	public void addKolicinaNet(int kolicinaNet) {
		this.kolicinaNet += kolicinaNet;
	}
	
	public void dodajKolicine(MesecniPodatkiTujina mpt){
		this.kolicinaKliciOdhodni += mpt.getKolicinaKliciOdhodni();
		this.kolicinaKliciDohodni += mpt.getKolicinaKliciDohodni();
		this.kolicinaSmsMms += mpt.getKolicinaSmsMms();
		this.kolicinaNet += mpt.getKolicinaNet();
	}

	public BasicDBObject dobiPodatkovniObjekt(){
		BasicDBObject doc = new BasicDBObject();
		
		if(this.id!=null){
			doc.put("_id", this.id);
		}
		doc.put("id_uporabnika", this.idUporabnika);
		doc.put("leto", this.leto);
		doc.put("mesec", this.mesec);
		doc.put("drzava", this.drzava);
		doc.put("kolicina_klici_odhodni", this.kolicinaKliciOdhodni);
		doc.put("kolicina_klici_dohodni", this.kolicinaKliciDohodni);
		doc.put("kolicina_sms_mms", this.kolicinaSmsMms);
		doc.put("kolicina_net", this.kolicinaNet);
		
		return doc;
	}

	public String zbirka() {
		return MesecniPodatkiTujina.ZBIRKA;
	}
}
