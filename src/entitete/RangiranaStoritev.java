package entitete;

public class RangiranaStoritev extends Storitev{
	
	private double cena;

	public RangiranaStoritev(Storitev storitev, double cena) {
		super(storitev.getId(), storitev.getIme(), storitev.getOperater(), storitev.getMesecnaNarocnina(), storitev.getObracunskiIntervalKlic(),
				storitev.getObracunskiIntervalEnota(), storitev.getObracunskiIntervalNet(),
				storitev.getCenaNaEnotoKlicDomace(), storitev.getCenaNaEnotoKlicTuje(),
				storitev.getCenaNaEnotoKlicStac(), storitev.getZakupljenaKolicinaKliciDomace(),
				storitev.getZakupljenaKolicinaKliciTujeStac(), storitev.getCenaNaEnotoSmsMms(),
				storitev.getZakupljenaKolicinaSmsMms(), storitev.getCenaNaEnotoNet(),
				storitev.getZakupljenaKolicinaNet(), storitev.getZakupljenaKolicinaSkupna());
		this.cena = cena;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

}
