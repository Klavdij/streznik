package entitete;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

public class RangiraneStoritve implements Entiteta{
	
	public static final String ZBIRKA="rangiraneStoritve";
	
	private ObjectId id;
	private ObjectId idUporabnika;
	private RangiranaStoritev[] storitve; 
	
	int stevec;

	public RangiraneStoritve(ObjectId idUporabnika, int steviloStoritev) {
		super();
		this.id = null;
		this.idUporabnika = idUporabnika;
		
		storitve=new RangiranaStoritev[steviloStoritev];
		stevec=0;
	}
	
	public RangiraneStoritve(BasicDBObject rs) {
		this.id = rs.getObjectId("_id");
		this.idUporabnika = rs.getObjectId("id_uporabnika");
		
		Storitev storitev;
		
		
		BasicDBList sto = (BasicDBList) rs.get("storitve");
		
		storitve=new RangiranaStoritev[sto.size()];
		
		for (int i = 0; i < storitve.length; i++) {
			storitev=new Storitev((BasicDBObject)sto.get(i));
			this.dodajStoritev(storitev, ((BasicDBObject)sto.get(i)).getDouble("cena"));
		}
		
	}
	
	public void dodajStoritev(Storitev storitev, double cena){
		
		RangiranaStoritev[]storitveTmp=new RangiranaStoritev[storitve.length];
		
		if(stevec<storitve.length){
			RangiranaStoritev s=new RangiranaStoritev(storitev, cena);
			
			boolean dodano=false;
			int st=0;
			
			for (int i = 0; i < stevec; i++) {
				if(s.getCena()<storitve[i].getCena() && !dodano){
					dodano=true;
					storitveTmp[st]=s;
					st++;
					storitveTmp[st]=storitve[i];
				}
				else{
					storitveTmp[st]=storitve[i];
				}
				st++;
			}
			
			if(!dodano){
				storitveTmp[stevec]=s;
			}
			
			stevec++;

			storitve=storitveTmp;
			
		}
	}
	
	public RangiranaStoritev[] vrniRangiraneStoritve(){
		
		return storitve;
	}
	
	public BasicDBObject dobiPodatkovniObjekt(){
		BasicDBObject doc = new BasicDBObject();
		BasicDBList sto = new BasicDBList();
		
		if(this.id!=null){
			doc.put("_id", this.id);
		}
		doc.put("id_uporabnika", this.idUporabnika);
		
		for (int i = 0; i < storitve.length; i++) {
			BasicDBObject s=new BasicDBObject();
			s=storitve[i].dobiPodatkovniObjekt();
			s.put("cena", storitve[i].getCena());
			
			sto.add(s);
		}
		
		doc.put("storitve", sto);
		
		return doc;
	}

	public String zbirka() {
		return RangiraneStoritve.ZBIRKA;
	}
	
}
