package entitete;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;

public class Odstopanje implements Entiteta{
	
	public static final String ZBIRKA="odstopanje";
	
	private ObjectId id;
	private ObjectId idUporabnika;
	private int leto;
	private int mesec;
	private int odstopanjeKliciDomace;
	private int odstopanjeKliciTuje;
	private int odstopanjeKliciStac;
	private int odstopanjeSmsMms;
	private int odstopanjeNet;
	
	public Odstopanje(ObjectId idUporabnika, int leto, int mesec,
			int odstopanjeKliciDomace, int odstopanjeKliciTuje,
			int odstopanjeKliciStac, int odstopanjeSmsMms, int odstopanjeNet) {
		super();
		this.id = null;
		this.idUporabnika = idUporabnika;
		this.leto = leto;
		this.mesec = mesec;
		this.odstopanjeKliciDomace = odstopanjeKliciDomace;
		this.odstopanjeKliciTuje = odstopanjeKliciTuje;
		this.odstopanjeKliciStac = odstopanjeKliciStac;
		this.odstopanjeSmsMms = odstopanjeSmsMms;
		this.odstopanjeNet = odstopanjeNet;
	}
	
	public Odstopanje(BasicDBObject o) {
		this.id = o.getObjectId("_id");
		this.idUporabnika = o.getObjectId("id_uporabnika");
		this.leto = o.getInt("leto");
		this.mesec = o.getInt("mesec");
		this.odstopanjeKliciDomace = o.getInt("odstopanje_klici_domace");
		this.odstopanjeKliciTuje = o.getInt("odstopanje_klici_tuje");
		this.odstopanjeKliciStac = o.getInt("odstopanje_klici_stac");
		this.odstopanjeSmsMms = o.getInt("odstopanje_sms_mms");
		this.odstopanjeNet = o.getInt("odstopanje_net");
	}
	
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public ObjectId getIdUporabnika() {
		return idUporabnika;
	}
	public void setIdUporabnika(ObjectId idUporabnika) {
		this.idUporabnika = idUporabnika;
	}
	public int getLeto() {
		return leto;
	}
	public void setLeto(int leto) {
		this.leto = leto;
	}
	public int getMesec() {
		return mesec;
	}
	public void setMesec(int mesec) {
		this.mesec = mesec;
	}
	public int getOdstopanjeKliciDomace() {
		return odstopanjeKliciDomace;
	}
	public void setOdstopanjeKliciDomace(int odstopanjeKliciDomace) {
		this.odstopanjeKliciDomace = odstopanjeKliciDomace;
	}
	public int getOdstopanjeKliciTuje() {
		return odstopanjeKliciTuje;
	}
	public void setOdstopanjeKliciTuje(int odstopanjeKliciTuje) {
		this.odstopanjeKliciTuje = odstopanjeKliciTuje;
	}
	public int getOdstopanjeKliciStac() {
		return odstopanjeKliciStac;
	}
	public void setOdstopanjeKliciStac(int odstopanjeKliciStac) {
		this.odstopanjeKliciStac = odstopanjeKliciStac;
	}
	public int getOdstopanjeSmsMms() {
		return odstopanjeSmsMms;
	}
	public void setOdstopanjeSmsMms(int odstopanjeSmsMms) {
		this.odstopanjeSmsMms = odstopanjeSmsMms;
	}
	public int getOdstopanjeNet() {
		return odstopanjeNet;
	}
	public void setOdstopanjeNet(int odstopanjeNet) {
		this.odstopanjeNet = odstopanjeNet;
	}	

	public BasicDBObject dobiPodatkovniObjekt(){
		BasicDBObject doc = new BasicDBObject();
		
		doc.put("id_uporabnika", this.idUporabnika);
		doc.put("leto", this.leto);
		doc.put("mesec", this.mesec);
		doc.put("odstopanje_klici_domace", this.odstopanjeKliciDomace);
		doc.put("odstopanje_klici_tuje", this.odstopanjeKliciTuje);
		doc.put("odstopanje_klici_stac", this.odstopanjeKliciStac);
		doc.put("odstopanje_sms_mms", this.odstopanjeSmsMms);
		doc.put("odstopanje_net", this.odstopanjeNet);
		
		return doc;
	}

	public String zbirka() {
		return Odstopanje.ZBIRKA;
	}

	public boolean equals(Object obj) {
		
		if(obj instanceof Odstopanje){
			Odstopanje o=(Odstopanje)obj;
			
			if(this.id!=null && o.id!=null){
				if(this.id==o.id){
					return true;
				}
			}
			else{
				if(this.idUporabnika==o.idUporabnika && this.leto==o.leto && this.mesec==o.mesec){
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	
}
