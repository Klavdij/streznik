package entitete;

import java.util.Date;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;

public class MesecnoPovprecje extends MesecniPodatki {
	
	public static final String ZBIRKA="mesecnoPovprecje";
	
	private Date zacetek;
	private Date konec;
	
	private int steviloZapisov;
	
	public MesecnoPovprecje(ObjectId idUporabnika, Date zacetek, Date konec,
			int kolicinaKliciDomace, int kolicinaKliciTuje, int kolicinaKliciStac,
			int kolicinaSmsMms, int kolicinaNet) {
		super(idUporabnika, 0, 0, kolicinaKliciDomace, kolicinaKliciTuje, kolicinaKliciStac, kolicinaSmsMms, kolicinaNet);
		this.id=null;
		this.zacetek = zacetek;
		this.konec = konec;
		
		this.steviloZapisov=1;
	}
	
	public MesecnoPovprecje(MesecniPodatki mp, Date zacetek, Date konec){
		super(mp.getIdUporabnika(), 0, 0, mp.getKolicinaKliciDomace(), mp.getKolicinaKliciTuje(), mp.getKolicinaKliciStac(), mp.getKolicinaSmsMms(), mp.getKolicinaNet());
		this.id=null;
		this.zacetek = zacetek;
		this.konec = konec;
		
		this.steviloZapisov=1;
	}
	
	public MesecnoPovprecje(BasicDBObject mp) {
		this.id = mp.getObjectId("_id");
		this.idUporabnika = mp.getObjectId("id_uporabnika");
		this.zacetek = mp.getDate("zacetek");
		this.konec = mp.getDate("konec");
		this.kolicinaKliciDomace = mp.getInt("kolicina_klici_domace");
		this.kolicinaKliciTuje = mp.getInt("kolicina_klici_tuje");
		this.kolicinaKliciStac = mp.getInt("kolicina_klici_stac");
		this.kolicinaSmsMms = mp.getInt("kolicina_sms_mms");
		this.kolicinaNet = mp.getInt("kolicina_net");
		
		this.steviloZapisov=1;
	}
	
	@Override
	public int getKolicinaKliciDomace() {
		return super.getKolicinaKliciDomace()/steviloZapisov;
	}
	
	public int getKolicinaKliciDomaceMin() {
		return (int) Math.round(this.getKolicinaKliciDomace()/60.0);
	}

	@Override
	public void setKolicinaKliciDomace(int kolicinaKliciDomace) {}

	@Override
	public int getKolicinaKliciTuje() {
		return super.getKolicinaKliciTuje()/steviloZapisov;
	}
	
	public int getKolicinaKliciTujeMin() {
		return (int) Math.round(this.getKolicinaKliciTuje()/60.0);
	}

	@Override
	public void setKolicinaKliciTuje(int kolicinaKliciTuje) {}

	@Override
	public int getKolicinaKliciStac() {
		return super.getKolicinaKliciStac()/steviloZapisov;
	}
	
	public int getKolicinaKliciStacMin() {
		return (int) Math.round(this.getKolicinaKliciStac()/60.0);
	}

	@Override
	public void setKolicinaKliciStac(int kolicinaKliciStac) {}

	@Override
	public int getKolicinaSmsMms() {
		return super.getKolicinaSmsMms()/steviloZapisov;
	}

	@Override
	public void setKolicinaSmsMms(int kolicinaSmsMms) {}

	@Override
	public int getKolicinaNet() {
		return super.getKolicinaNet()/steviloZapisov;
	}
	
	public int getKolicinaNetMB() {
		return (int) Math.round(this.getKolicinaNet()/1024.0);
	}

	@Override
	public void setKolicinaNet(int kolicinaNet) {}

	public Date getZacetek() {
		return zacetek;
	}

	public void setZacetek(Date zacetek) {
		this.zacetek = zacetek;
	}

	public Date getKonec() {
		return konec;
	}

	public void setKonec(Date konec) {
		this.konec = konec;
	}

	public void dodajKolicine(MesecniPodatki mp){
		this.kolicinaKliciDomace = this.kolicinaKliciDomace + mp.getKolicinaKliciDomace();
		this.kolicinaKliciTuje = this.kolicinaKliciTuje + mp.getKolicinaKliciTuje();
		this.kolicinaKliciStac = this.kolicinaKliciStac + mp.getKolicinaKliciStac();
		this.kolicinaSmsMms = this.kolicinaSmsMms + mp.getKolicinaSmsMms();
		this.kolicinaNet = this.kolicinaNet + mp.getKolicinaNet();
		
		this.steviloZapisov++;
	}
	
	private void nastaviPovprecje(){
		this.kolicinaKliciDomace = this.kolicinaKliciDomace / steviloZapisov;
		this.kolicinaKliciTuje = this.kolicinaKliciTuje / steviloZapisov;
		this.kolicinaKliciStac = this.kolicinaKliciStac / steviloZapisov;
		this.kolicinaSmsMms = this.kolicinaSmsMms / steviloZapisov;
		this.kolicinaNet = this.kolicinaNet / steviloZapisov;
		
		this.steviloZapisov=1;
	}

	@Override
	public BasicDBObject dobiPodatkovniObjekt() {
		
		nastaviPovprecje();
		
		BasicDBObject doc = new BasicDBObject();
		
		if(this.id!=null){
			doc.put("_id", this.id);
		}
		doc.put("id_uporabnika", this.idUporabnika);
		doc.put("zacetek", this.zacetek);
		doc.put("konec", this.konec);
		doc.put("kolicina_klici_domace", this.kolicinaKliciDomace);
		doc.put("kolicina_klici_tuje", this.kolicinaKliciTuje);
		doc.put("kolicina_klici_stac", this.kolicinaKliciStac);
		doc.put("kolicina_sms_mms", this.kolicinaSmsMms);
		doc.put("kolicina_net", this.kolicinaNet);
		
		return doc;
	}

	public String zbirka() {
		return MesecnoPovprecje.ZBIRKA;
	}
	
}
