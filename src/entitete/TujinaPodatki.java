package entitete;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;

public class TujinaPodatki implements Entiteta{

	public static final String ZBIRKA="tujinaPodatki";
	
	private ObjectId id;
	private ObjectId idUporabnika;
	private int steviloOdstopanj;
	private int steviloDrzav;
	private double skupnaCena;
	private int koncnoLeto;
	private int koncniMesec;
	
	public TujinaPodatki(ObjectId idUporabnika, int koncnoLeto, int koncniMesec) {
		super();
		this.idUporabnika = idUporabnika;
		this.koncnoLeto = koncnoLeto;
		this.koncniMesec = koncniMesec;
		this.steviloOdstopanj = 0;
		this.steviloDrzav = 0;
		this.skupnaCena = 0.0;
	}

	public TujinaPodatki(ObjectId idUporabnika, int steviloOdstopanj,
			int steviloDrzav, double skupnaCena, int koncnoLeto, int koncniMesec) {
		super();
		this.id=null;
		this.idUporabnika = idUporabnika;
		this.steviloOdstopanj = steviloOdstopanj;
		this.steviloDrzav = steviloDrzav;
		this.skupnaCena = skupnaCena;
		this.koncnoLeto = koncnoLeto;
		this.koncniMesec = koncniMesec;
	}
	
	public TujinaPodatki(BasicDBObject tp) {
		this.id = tp.getObjectId("_id");
		this.idUporabnika = tp.getObjectId("id_uporabnika");
		this.steviloOdstopanj = tp.getInt("stevilo_odstopanj");
		this.steviloDrzav = tp.getInt("stevilo_drzav");
		this.skupnaCena = tp.getDouble("skupna_cena");
		this.koncnoLeto = tp.getInt("koncno_leto");
		this.koncniMesec = tp.getInt("koncni_mesec");
	}
	
	public ObjectId getId() {
		return id;
	}

	public int getKoncnoLeto() {
		return koncnoLeto;
	}

	public void setKoncnoLeto(int koncnoLeto) {
		this.koncnoLeto = koncnoLeto;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ObjectId getIdUporabnika() {
		return idUporabnika;
	}

	public void setIdUporabnika(ObjectId idUporabnika) {
		this.idUporabnika = idUporabnika;
	}

	public int getKoncniMesec() {
		return koncniMesec;
	}

	public void setKoncniMesec(int koncniMesec) {
		this.koncniMesec = koncniMesec;
	}

	public int getSteviloOdstopanj() {
		return steviloOdstopanj;
	}

	public void setSteviloOdstopanj(int steviloOdstopanj) {
		this.steviloOdstopanj = steviloOdstopanj;
	}
	
	public void addSteviloOdstopanj(int steviloOdstopanj) {
		this.steviloOdstopanj += steviloOdstopanj;
	}

	public int getSteviloDrzav() {
		return steviloDrzav;
	}

	public void setSteviloDrzav(int steviloDrzav) {
		this.steviloDrzav = steviloDrzav;
	}

	public void addSteviloDrzav(int steviloDrzav) {
		this.steviloDrzav += steviloDrzav;
	}
	
	public double getSkupnaCena() {
		return skupnaCena;
	}

	public void setSkupnaCena(double skupnaCena) {
		this.skupnaCena = skupnaCena;
	}
	
	public void addSkupnaCena(double skupnaCena) {
		this.skupnaCena += skupnaCena;
	}
	
	public double getPovprecnaMesecnaCena(){
		if(steviloOdstopanj>0){
			return skupnaCena/steviloOdstopanj;
		}
		return 0.0;
	}

	@Override
	public BasicDBObject dobiPodatkovniObjekt() {
		BasicDBObject doc = new BasicDBObject();
		
		if(this.id!=null){
			doc.put("_id", this.id);
		}
		doc.put("id_uporabnika", this.idUporabnika);
		doc.put("stevilo_odstopanj", this.steviloOdstopanj);
		doc.put("stevilo_drzav", this.steviloDrzav);
		doc.put("skupna_cena", this.skupnaCena);
		doc.put("koncno_leto", this.koncnoLeto);
		doc.put("koncni_mesec", this.koncniMesec);
		
		return doc;
	}
	
	public String zbirka() {
		return MesecniPodatki.ZBIRKA;
	}


}
