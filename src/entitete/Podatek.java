package entitete;

import java.util.Date;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;

public class Podatek {
	
	public static final String ZBIRKA="podatki";
	
	private ObjectId id;
	private ObjectId idUporabnika;
	private Date datum;
	private char tip;
	private int odhodniDohodni;
	private int stevilka;
	private int trajanje;
	private int kolicina;
	
	public Podatek(ObjectId idUporabnika, Date datum, char tip, 
			int odhodniDohodni, int stevilka,
			int trajanje, int kolicina) {
		super();
		this.id=null;
		this.idUporabnika = idUporabnika;
		this.datum = datum;
		this.tip = tip;
		this.odhodniDohodni = odhodniDohodni;
		this.stevilka = stevilka;
		this.trajanje = trajanje;
		this.kolicina = kolicina;
	}
	
	public Podatek(BasicDBObject p) {
		this.id=p.getObjectId("_id");
		this.idUporabnika = p.getObjectId("id_uporabnika");
		this.datum = p.getDate("datum");
		this.tip = p.getString("tip").charAt(0);
		this.odhodniDohodni = p.getInt("odhodni_dohodni");
		this.stevilka = p.getInt("stevilka");
		this.trajanje = p.getInt("trajanje");
		this.kolicina = p.getInt("kolicina");
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ObjectId getIdUporabnika() {
		return idUporabnika;
	}

	public void setIdUporabnika(ObjectId idUporabnika) {
		this.idUporabnika = idUporabnika;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public char getTip() {
		return tip;
	}

	public void setTip(char tip) {
		this.tip = tip;
	}

	public int getOdhodniDohodni() {
		return odhodniDohodni;
	}

	public void setOdhodniDohodni(int odhodniDohodni) {
		this.odhodniDohodni = odhodniDohodni;
	}

	public int getStevilka() {
		return stevilka;
	}

	public void setStevilka(int stevilka) {
		this.stevilka = stevilka;
	}

	public int getTrajanje() {
		return trajanje;
	}

	public void setTrajanje(int trajanje) {
		this.trajanje = trajanje;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}
}
