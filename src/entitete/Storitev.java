package entitete;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;

public class Storitev implements Entiteta{
	
	public static final String ZBIRKA="storitev";
	
	private ObjectId id;
	private String ime;
	private String operater;
	private double mesecnaNarocnina;
	private int obracunskiIntervalKlic;
	private int obracunskiIntervalEnota;
	private int obracunskiIntervalNet;
	private double cenaNaEnotoKlicDomace;
	private double cenaNaEnotoKlicTuje;
	private double cenaNaEnotoKlicStac;
	private int zakupljenaKolicinaKliciDomace;
	private int zakupljenaKolicinaKliciTujeStac;
	private double cenaNaEnotoSmsMms;
	private int zakupljenaKolicinaSmsMms;
	private double cenaNaEnotoNet;
	private int zakupljenaKolicinaNet;
	private int zakupljenaKolicinaSkupna;
	
	public Storitev(ObjectId id, String ime, String operater,
			double mesecnaNarocnina, int obracunskiIntervalKlic,
			int obracunskiIntervalEnota, int obracunskiIntervalNet,
			double cenaNaEnotoKlicDomace, double cenaNaEnotoKlicTuje,
			double cenaNaEnotoKlicStac, int zakupljenaKolicinaKliciDomace,
			int zakupljenaKolicinaKliciTujeStac, double cenaNaEnotoSmsMms,
			int zakupljenaKolicinaSmsMms, double cenaNaEnotoNet,
			int zakupljenaKolicinaNet, int zakupljenaKolicinaSkupna) {
		super();
		this.id = id;
		this.ime = ime;
		this.operater = operater;
		this.mesecnaNarocnina = mesecnaNarocnina;
		this.obracunskiIntervalKlic = obracunskiIntervalKlic;
		this.obracunskiIntervalEnota = obracunskiIntervalEnota;
		this.obracunskiIntervalNet = obracunskiIntervalNet;
		this.cenaNaEnotoKlicDomace = cenaNaEnotoKlicDomace;
		this.cenaNaEnotoKlicTuje = cenaNaEnotoKlicTuje;
		this.cenaNaEnotoKlicStac = cenaNaEnotoKlicStac;
		this.zakupljenaKolicinaKliciDomace = zakupljenaKolicinaKliciDomace;
		this.zakupljenaKolicinaKliciTujeStac = zakupljenaKolicinaKliciTujeStac;
		this.cenaNaEnotoSmsMms = cenaNaEnotoSmsMms;
		this.zakupljenaKolicinaSmsMms = zakupljenaKolicinaSmsMms;
		this.cenaNaEnotoNet = cenaNaEnotoNet;
		this.zakupljenaKolicinaNet = zakupljenaKolicinaNet;
		this.zakupljenaKolicinaSkupna = zakupljenaKolicinaSkupna;
	}

	public Storitev(BasicDBObject s) {
		super();
		this.id = s.getObjectId("_id");
		this.ime = s.getString("ime");
		this.operater = s.getString("operater");
		this.mesecnaNarocnina = s.getDouble("mesecna_narocnina");
		this.obracunskiIntervalKlic = s.getInt("obracunski_interval_klic");
		this.obracunskiIntervalEnota = s.getInt("obracunski_interval_enota");
		this.obracunskiIntervalNet = s.getInt("obracunski_interval_net");
		this.cenaNaEnotoKlicDomace = s.getDouble("cena_na_enoto_klic_domace");
		this.cenaNaEnotoKlicTuje = s.getDouble("cena_na_enoto_klic_tuje");
		this.cenaNaEnotoKlicStac = s.getDouble("cena_na_enoto_klic_stac");
		this.zakupljenaKolicinaKliciDomace = s.getInt("zakupljena_kolicina_klici_domace");
		this.zakupljenaKolicinaKliciTujeStac = s.getInt("zakupljena_kolicina_klici_tuje_stac");
		this.cenaNaEnotoSmsMms = s.getDouble("cena_na_enoto_sms_mms");
		this.zakupljenaKolicinaSmsMms = s.getInt("zakupljena_kolicina_sms_mms");
		this.cenaNaEnotoNet = s.getDouble("cena_na_enoto_net");
		this.zakupljenaKolicinaNet = s.getInt("zakupljena_kolicina_net");
		this.zakupljenaKolicinaSkupna = s.getInt("zakupljena_kolicina_skupna");
	}

	public ObjectId getId() {
		return id;
	}

	public String getIme() {
		return ime;
	}

	public String getOperater() {
		return operater;
	}

	public double getMesecnaNarocnina() {
		return mesecnaNarocnina;
	}

	public int getObracunskiIntervalKlic() {
		return obracunskiIntervalKlic;
	}

	public int getObracunskiIntervalEnota() {
		return obracunskiIntervalEnota;
	}

	public int getObracunskiIntervalNet() {
		return obracunskiIntervalNet;
	}

	public double getCenaNaEnotoKlicDomace() {
		return cenaNaEnotoKlicDomace;
	}

	public double getCenaNaEnotoKlicTuje() {
		return cenaNaEnotoKlicTuje;
	}

	public double getCenaNaEnotoKlicStac() {
		return cenaNaEnotoKlicStac;
	}

	public int getZakupljenaKolicinaKliciDomace() {
		return zakupljenaKolicinaKliciDomace;
	}

	public int getZakupljenaKolicinaKliciTujeStac() {
		return zakupljenaKolicinaKliciTujeStac;
	}

	public double getCenaNaEnotoSmsMms() {
		return cenaNaEnotoSmsMms;
	}

	public int getZakupljenaKolicinaSmsMms() {
		return zakupljenaKolicinaSmsMms;
	}

	public double getCenaNaEnotoNet() {
		return cenaNaEnotoNet;
	}

	public int getZakupljenaKolicinaNet() {
		return zakupljenaKolicinaNet;
	}

	public int getZakupljenaKolicinaSkupna() {
		return zakupljenaKolicinaSkupna;
	}

	@Override
	public BasicDBObject dobiPodatkovniObjekt() {
		BasicDBObject doc = new BasicDBObject();
		
		if(this.id!=null){
			doc.put("_id", this.id);
		}
		doc.put("ime", this.ime);
		doc.put("operater", this.operater);
		doc.put("mesecna_narocnina", this.mesecnaNarocnina);
		doc.put("obracunski_interval_klic", this.obracunskiIntervalKlic);
		doc.put("obracunski_interval_enota", this.obracunskiIntervalEnota);
		doc.put("cena_na_enoto_klic_domace", this.cenaNaEnotoKlicDomace);
		doc.put("cena_na_enoto_klic_tuje", this.cenaNaEnotoKlicDomace);
		doc.put("cena_na_enoto_klic_stac", this.cenaNaEnotoKlicStac);
		doc.put("zakupljena_kolicina_klici_domace", this.zakupljenaKolicinaKliciDomace);
		doc.put("zakupljena_kolicina_klici_tuje_stac", this.zakupljenaKolicinaKliciTujeStac);
		doc.put("cena_na_enoto_sms_mms", this.cenaNaEnotoSmsMms);
		doc.put("zakupljena_kolicina_sms_mms", this.zakupljenaKolicinaSmsMms);
		doc.put("cena_na_enoto_net", this.cenaNaEnotoNet);
		doc.put("obracunski_interval_net", this.obracunskiIntervalNet);
		doc.put("zakupljena_kolicina_net", this.zakupljenaKolicinaNet);
		doc.put("zakupljena_kolicina_skupna", this.zakupljenaKolicinaSkupna);
		
		return doc;
	}
	
	public String zbirka() {
		return Storitev.ZBIRKA;
	}

}
