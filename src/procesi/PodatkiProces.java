package procesi;

import java.util.Date;

import org.bson.types.ObjectId;

import orodja.Baza;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

public class PodatkiProces {
	
	public static String podatkiJSON(String idUporabnika, char tip, boolean vsebujeUporabnika, int odhodniDohodni){
		
		ObjectId idUpo=new ObjectId(idUporabnika);
		
		BasicDBObject pogoji;
		
		if(vsebujeUporabnika){
			pogoji=new BasicDBObject("id_uporabnika", idUpo);
		}
		else{
			pogoji=new BasicDBObject("id_uporabnika",new BasicDBObject("$ne", idUpo));
		}
		
		if(odhodniDohodni!=-1 && tip!='n'){
			pogoji.append("odhodni_dohodni", odhodniDohodni);
		}		
		
		String json="[";
		
		Baza baza=new Baza();
		
		BasicDBList list=baza.dobiPodatke("podatki", pogoji.append("tip", tip),new BasicDBObject("datum", 1));
		BasicDBObject o;
		
		for (int i = 0; i < list.size(); i++) {
			o=(BasicDBObject) list.get(i);
			
			Date d=o.getDate("datum");
			json+="["+d.getTime();
			
			
			if(tip=='k'){
				int t=o.getInt("trajanje")/60;
				json+=","+t+"]";
			}
			else if(tip=='n'){
				double kol=o.getDouble("kolicina");
				kol=(int)Math.round((kol/1024.0));
				
				json+=","+kol+"]";
			}
			else{
				json+=",1]";
			}
			
			
			if(i+1!=list.size()){
				json+=",";
			}
		}
		
		json+="]";
		
		baza.zapriPovezavo();
		
		return json;
	}
	
	public static int stUporabnikov(){
		int st=0;
		
		Baza b=new Baza();
		
		st=b.dobiStPodatkov("uporabnik", null)-1;
		
		b.zapriPovezavo();
		
		return st;
	}
}
