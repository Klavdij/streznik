package procesi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.bson.types.ObjectId;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;

import orodja.Baza;
import orodja.Operater;

public class PorabaProces {

	public static String vrniTrenutniMesec(){
		
		SimpleDateFormat sdf=new SimpleDateFormat("M");
		
		int mesec=Integer.parseInt(sdf.format(new Date()));
		
		String [] meseci={"Januar","Februar","Marec","April","Maj","Junij","Julij","Avgust","September","Oktober","November","December"};
		
		return meseci[mesec-1]; 
		
	}
	
	public static String porabljenaSredstva(String uporabnik){
		
		Baza b=new Baza();
		
		ObjectId idUpo=new ObjectId(uporabnik);
		
		GregorianCalendar gc=new GregorianCalendar();
		
		int year=gc.get(Calendar.YEAR);
		int month=gc.get(Calendar.MONTH);
		int days=gc.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		GregorianCalendar from=new GregorianCalendar(year,month,1,0,0,0);
		GregorianCalendar to=new GregorianCalendar(year,month,days,0,0,0);		
		
		// Stevilke operaterjev
		int domacOperater=Operater.vrniOperaterja(b.dobiStoritevUporabnika(idUpo).getString("operater"));
		int[] domaceStevilke=Operater.vrniStevilke(domacOperater);
		int[] tujeStevilke=Operater.vrniStevilkeOstalih(domacOperater);
		
		String output="";
		int[] opcije={-1,1,0};
		
		for (int i = 0; i < opcije.length; i++) {
			
			// Skupni pogoji
			BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUpo).
					append("datum", new BasicDBObject("$gte", from.getTime()).append("$lte", to.getTime()));
			
			// SMS/MMS
			dodajPogoje(pogoji, "s", null, opcije[i]);
			int smsi=b.dobiStPodatkov("podatki", pogoji);
			
			dodajPogoje(pogoji, "m", null, opcije[i]);
			
			int mmsi=b.dobiStPodatkov("podatki", pogoji);
			
			
			// NET
			dodajPogoje(pogoji, "n", null, opcije[i]);
			
			int net=b.dobiSestevekPodatkov("podatki", pogoji);
			
			
			// Klici domace	
			dodajPogoje(pogoji, "k", domaceStevilke, opcije[i]);
			
			int kliciDomace=b.dobiSestevekPodatkov("podatki", pogoji);
			
			
			// Klici tuje
			dodajPogoje(pogoji, "k", tujeStevilke, opcije[i]);
			
			int kliciTuje=b.dobiSestevekPodatkov("podatki", pogoji);
			
			output+=smsi+mmsi+","+kliciDomace+","+kliciTuje+","+net;
			
			if(i<opcije.length-1){
				output+=";";				
			}
			
		}
		
		b.zapriPovezavo();
		
		return output;
	}
	
	private static void dodajPogoje(BasicDBObject pogoji, String tip,int []stevilke,int tipKlica){
		
		pogoji.append("tip", tip);
		
		if(tipKlica!=-1){
			if(tip.equals("n")){
				pogoji.append("odhodni_dohodni", 0);
			}
			else{
				pogoji.append("odhodni_dohodni", tipKlica);
			}
		}
		
		if(stevilke!=null){
			pogoji.append("stevilka",new BasicDBObject("$in", stevilke));
		}
	}
	
	public static String procentKlicevVOmrezja(String uporabnik){
		
		Baza b=new Baza();
		
		String data="[";
		JsonObject serija=new JsonObject();
		boolean jePrvi=true;
		
		ObjectId idUpo=new ObjectId(uporabnik);
		
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUpo).append("tip", "k");
		
		String [] operaterji=Operater.imenaOperaterjev;
		int [] kolicina=new int[operaterji.length];
		int skupaj=0;
		double procent;
		
		// Pridobi podatke
		for (int i = 0; i < operaterji.length; i++) {
			
			pogoji.append("stevilka",new BasicDBObject("$in", Operater.stevilke[i]));
			
			kolicina[i]=b.dobiSestevekPodatkov("podatki", pogoji);
			
			skupaj+=kolicina[i];
		}
		
		
		// Zracunaj procente
		for (int i = 0; i < kolicina.length; i++) {
			
			procent=(100.0/skupaj)*kolicina[i];
			
			if(!jePrvi){
				data+=",";
			}
			else{
				jePrvi=false;
			}
			
			if(procent>0.0){
				data+=String.format(Locale.US,"[%s,%.2f]", operaterji[i],procent);
			}
			
			
		}
		
		data+="]";
		serija.addProperty("name", "Dele� omre�ja");
		serija.add("data",new JsonParser().parse(data));
		
		b.zapriPovezavo();
		
		return serija.toString();
	}
	
public static String procentKlicevVOmrezjaPoLetih(String uporabnik){
		
		Baza baza=new Baza();
		
		JsonObject serija;
		JsonArray serije=new JsonArray();
		boolean jePrvi;
		
		String data="[";
		
		ObjectId idUpo=new ObjectId(uporabnik);
		
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUpo).append("tip", "k");
		
		Date prviDatum=baza.dobiEnega("podatki", pogoji, null, new BasicDBObject("datum", 1)).getDate("datum");
		Date zadnjiDatum=baza.dobiEnega("podatki", pogoji, null, new BasicDBObject("datum", -1)).getDate("datum");
		
		GregorianCalendar gcZ = new GregorianCalendar();
		gcZ.setTime(prviDatum);
		GregorianCalendar gcK = new GregorianCalendar();
		gcK.setTime(zadnjiDatum);
		
		int prvoLeto=gcZ.get(GregorianCalendar.YEAR);
		
		int zadnjLeto=gcK.get(GregorianCalendar.YEAR);

		while(prvoLeto<=zadnjLeto){
			
			jePrvi=true;
			
			serija=new JsonObject();
		
			String [] operaterji=Operater.imenaOperaterjev;
			int [] kolicina=new int[operaterji.length];
			int skupaj=0;
			double procent;
			
			// Pridobi podatke
			for (int i = 0; i < operaterji.length; i++) {
				pogoji=new BasicDBObject("id_uporabnika", idUpo).append("tip", "k");
				
				pogoji.append("stevilka",new BasicDBObject("$in", Operater.stevilke[i]));
				
				GregorianCalendar from=new GregorianCalendar(prvoLeto,0,0,0,0,0);
				
				GregorianCalendar to=new GregorianCalendar(prvoLeto,11,31,0,0,0);
				
				pogoji.append("datum", new BasicDBObject("$gte", from.getTime()).append("$lte", to.getTime()));
				
				kolicina[i]=baza.dobiSestevekPodatkov("podatki", pogoji);
				
				skupaj+=kolicina[i];
			}
			
			
			// Zracunaj procente
			for (int i = 0; i < kolicina.length; i++) {
				
				procent=(100.0/skupaj)*kolicina[i];
				
				if(procent>0.0){
					
					if(!jePrvi){
						data+=",";
					}
					else{
						jePrvi=false;
					}
					
					data+=String.format(Locale.US,"[%s,%.2f]", operaterji[i],procent);
				}
			}
			
			data+="]";
			
			serija.addProperty("name", prvoLeto+"");			
			serija.add("data", new JsonParser().parse(data));
			
			data="[";
			
			serije.add(serija);
			
			prvoLeto++;
		}
		
		baza.zapriPovezavo();
		
		return serije.toString();
	}
}
