package procesi;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.bson.types.ObjectId;

import orodja.Baza;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import entitete.RangiranaStoritev;
import entitete.Storitev;

public class AnalizaProces {
	
	public static String[] vrniOperaterje(){
		int st=0;
		String [] operaterji=null;
		ArrayList<String> operList=new ArrayList<String>();
		
		Baza baza=new Baza();
		
		BasicDBObject order=new BasicDBObject("operater", 1);
		
		BasicDBList storitve=baza.dobiPodatke(Storitev.ZBIRKA, null, order);
		
		if(storitve!=null){
			
			operList.add(((BasicDBObject)storitve.get(st)).getString("operater"));
			
			for (int i = 1; i < storitve.size(); i++) {
				if(!operList.get(st).equals(((BasicDBObject)storitve.get(i)).getString("operater"))){
					operList.add(((BasicDBObject)storitve.get(i)).getString("operater"));
					st++;
				}
			}
			
		}
		
		operaterji=operList.toArray(new String[operList.size()]);
		
		baza.zapriPovezavo();
		
		return operaterji;
	}
	
	public static boolean vsebujeOperaterja(String operater, String []operaterji){
		
		for (int i = 0; i < operaterji.length; i++) {
			if(operater.equals(operaterji[i])){
				return true;
			}
		}
		
		return false;
	}
	
	public static String prikazSeznamaStoritev(RangiranaStoritev[] storitve, String idStoritveUporabnika, String[] operaterjiIzbrani, String narocnina, String prikaz) {
		int index=1;
		String izpis="";
		int iStoritve= vrniIndexUpoStoritve(storitve, idStoritveUporabnika, operaterjiIzbrani, narocnina);
		boolean jeIzpisana=false;
		
		for (int i = 0; i < storitve.length; i++){
			
			if(prikaziStoritev(storitve[i], operaterjiIzbrani, narocnina) || idStoritveUporabnika.equals(storitve[i].getId().toString())){
				if(prikaz.equals("vse")){
					izpis+=izpisiVrstico(storitve, idStoritveUporabnika, index, i);
					index++;
				}
				else{
					
					if(index<6){
						
						izpis+=izpisiVrstico(storitve, idStoritveUporabnika, index, i);
						index++;
					}
					else if(index>iStoritve-3 && index<iStoritve+3){
						
						izpis+=izpisiVrstico(storitve, idStoritveUporabnika, index, i);
						index++;
					}
					else if(index>iStoritve+3){
						
						izpis+="<li class='dots'>&bull;&bull;&bull;</li>";
						break;
					}
					else{
						index++;
					}
					
					if(index==iStoritve){
						jeIzpisana=true;
					}	
					
					if(index==6 && index<iStoritve-2){
						izpis+="<li class='dots'>&bull;&bull;&bull;</li>";
						
						if(jeIzpisana){
							break;
						}
					}
				}
			}
		}
		
		return izpis;
	}
	
	private static String izpisiVrstico(RangiranaStoritev[] storitve, String idStoritveUporabnika, int index, int i){
		String idStoritve=storitve[i].getId().toString();
		DecimalFormat df = new DecimalFormat("0.##");
		String cena="Mese�ni stro�ek: "+df.format((Double)storitve[i].getCena())+" �";
		return  "<li "
				+ (idStoritveUporabnika.equals(idStoritve)?"style='color: #4C9CF1;'":"")
				+ ">"
				+ index
				+ ". "
				+ "<a title='"+cena+"' "
				+ "href='?page=storitev&id="
				+ storitve[i].getId()
				+ "'><img src=icons/"
				+ storitve[i].getOperater()
				+ ".png><b> "
				+ storitve[i].getIme()
				+ "</b></a></li>";
	}
	
	private static int vrniIndexUpoStoritve(RangiranaStoritev[] storitve, String idStoritveUporabnika, String[] operaterji, String narocnina){
		int index=1;
		for (int i = 0; i < storitve.length; i++) {
			if(prikaziStoritev(storitve[i], operaterji, narocnina) || idStoritveUporabnika.equals(storitve[i].getId().toString())){
				if(idStoritveUporabnika.equals(storitve[i].getId().toString())){
					return index;
				}				
				index++;
			}
		}
		
		return index;
	}
	
	public static boolean prikaziStoritev(RangiranaStoritev storitev,String []operaterji, String narocnina){
		
		if(vsebujeOperaterja(storitev.getOperater(), operaterji)){
			if(narocnina.equals("vsi")){
				return true;
			}
			else if(narocnina.equals("z") && (storitev.getMesecnaNarocnina()>0.0)){
				return true;
			}
			else if(narocnina.equals("brez") && (storitev.getMesecnaNarocnina()==0.0)){
				return true;
			}
		}
		
		return false;
	}

	public static String grafStoritev(String idUporabnika){
		
		ObjectId idUpo=new ObjectId(idUporabnika);
		
		BasicDBObject storitev;
		JsonObject data;
		JsonArray series=new JsonArray();
		String operater;
		HashMap<String, ArrayList<JsonObject>> rangiraneStoritve=new HashMap<String, ArrayList<JsonObject>>();
		ArrayList<JsonObject> dataAL;
		
		Baza baza=new Baza();
		
		BasicDBList rsList=baza.dobiPodatke(Storitev.ZBIRKA, null, null);
				
		BasicDBObject storitevUporabnika=baza.dobiStoritevUporabnika(idUpo);
		
		for (int i = 0; i < rsList.size(); i++) {
			
			storitev=(BasicDBObject)rsList.get(i);
			
			data=new JsonObject();
			
			operater=storitev.getString("operater");
			
			data.addProperty("name", storitev.getString("ime"));
			data.addProperty("x", storitev.getDouble("mesecna_narocnina"));
			
			if(rangiraneStoritve.containsKey(operater)){
				
				dataAL=rangiraneStoritve.get(operater);
				dataAL.add(data);
				
			}
			else{
				
				dataAL=new ArrayList<JsonObject>();
				dataAL.add(data);
				rangiraneStoritve.put(operater, dataAL);
				
			}
			
		}
		
		baza.zapriPovezavo();
		
		String [] operaterji=AnalizaProces.vrniOperaterje();
		
		for (int i = 0; i < operaterji.length; i++) {
			
			dataAL=rangiraneStoritve.get(operaterji[i]);
			
			for (int j = 0; j < dataAL.size(); j++) {
				
				data=dataAL.get(j);
				
				data.addProperty("y", i);
				data.addProperty("z", 1);

				String ime=data.get("name").getAsString();
				
				if(ime.equals(storitevUporabnika.get("ime"))){
					data.addProperty("fillColor", "#30DA60");
				}
				
				series.add(data);
			}
			
		}

		return series.toString();
	}
}
