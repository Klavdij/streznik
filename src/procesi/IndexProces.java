package procesi;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import entitete.RezultatStroskov;
import entitete.Storitev;
import orodja.Baza;
import orodja.Gostovanje;
import orodja.Lokacija;
import orodja.Operater;
import rangiranje.rezultati.Rezultat;

public class IndexProces {
	
	public static String [][] vrniUporabnike(){
		String [][] uporabniki;
		
		Baza baza=new Baza();
		BasicDBList listUpo=baza.dobiPodatke("uporabnik", null, new BasicDBObject("ime", 1).append("priimek", 1));
		
		uporabniki=new String[listUpo.size()][2];
		
		for (int i = 0; i < uporabniki.length; i++) {
			BasicDBObject o=(BasicDBObject) listUpo.get(i);
			uporabniki[i][0]=o.getString("ime")+" "+o.getString("priimek");
			uporabniki[i][1]=o.getString("_id");
		}
		
		baza.zapriPovezavo();
		
		return uporabniki;
	}
	
	public static BasicDBObject vrniStoritev(String uporabnik){
		
		BasicDBObject storitev;
		Baza baza=new Baza();
		
		ObjectId idUpo=new ObjectId(uporabnik);
		
		storitev=baza.dobiStoritevUporabnika(idUpo);
		
		baza.zapriPovezavo();
		
		return storitev;
	}
	
	public static BasicDBObject vrniStoritevIzId(String idStoritve){
		
		BasicDBObject storitev;
		Baza baza=new Baza();
		
		ObjectId id=new ObjectId(idStoritve);
		BasicDBObject pogoji=new BasicDBObject("_id", id);
		
		storitev=baza.dobiEnega(Storitev.ZBIRKA, pogoji, null, null);
		
		baza.zapriPovezavo();
		
		return storitev;
	}
	
	public static Rezultat vrniCenePorabe(String uporabnik){
		Rezultat rezultat=new Rezultat();
		Baza baza=new Baza();
		Lokacija lokacija=new Lokacija();
		Gostovanje gostovanje=new Gostovanje();
		
		DecimalFormat df = new DecimalFormat("#,###.##");
		
		ObjectId idUpo=new ObjectId(uporabnik);
		
		GregorianCalendar gc=new GregorianCalendar();
		
		int year=gc.get(Calendar.YEAR);
		int month=gc.get(Calendar.MONTH);
		int days=gc.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		GregorianCalendar from=new GregorianCalendar(year,month,1,0,0,0);
		GregorianCalendar to=new GregorianCalendar(year,month,days,0,0,0);	
		
		BasicDBObject storitev=baza.dobiStoritevUporabnika(idUpo);
		BasicDBObject upo=baza.dobiEnega("uporabnik", new BasicDBObject("_id", idUpo),null,null);
		String drzavaUporabnika=((BasicDBObject)upo.get("naslov")).getString("drzava");
		int operaterUporanika=Operater.vrniOperaterja(storitev.getString("operater"));
		
		
		double mesecnaNarocnina=storitev.getDouble("mesecna_narocnina");
		double cenaEnot=0.0;
		double cenaTujina=0.0;
		
		// Skupni pogoji
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUpo).
				append("datum", new BasicDBObject("$gte", from.getTime()).append("$lte", to.getTime()));

		BasicDBObject podatek;
		
		double lokacijaX;
		double lokacijaY;
		String drzava="";
		
		char tip;
		int odhodni;
		int stevilka;
		int operaterSt;
		
		int smsMms=0;
		int kliciDomace=0;
		int kliciTuje=0;
		int kliciStac=0;
		int net=0;
		
		int smsMmsTujina=0;
		int kliciOdhodniTujina=0;
		int kliciDohodniTujina=0;
		int netTujina=0;
		
		BasicDBList listPodatkov=baza.dobiPodatke("podatki", pogoji, null);
		
		for (int j = 0; j < listPodatkov.size(); j++) {
			
			podatek=(BasicDBObject)listPodatkov.get(j);
			
			lokacijaX=podatek.getDouble("lokacija_x");
			lokacijaY=podatek.getDouble("lokacija_y");
			
			drzava=lokacija.getCountry(lokacijaX, lokacijaY);
			
			if(drzava.equals(drzavaUporabnika)){
				
				tip=podatek.getString("tip").charAt(0);
				odhodni=podatek.getInt("odhodni_dohodni");
				
				switch (tip) {
				case 's':
					if(odhodni==1){
						smsMms++;
					}
					break;
				case 'm':
					if(odhodni==1){
						smsMms++;
					}
					break;
				case 'n':
					net+=podatek.getInt("kolicina");
					break;
				case 'k':
					if(odhodni==1){
						
						stevilka=podatek.getInt("stevilka");
						operaterSt=Operater.vrniOperaterja(stevilka);
						
						if(operaterSt==Operater.STACIONARNA){
							kliciStac+=podatek.getInt("trajanje");
						}
						else if(operaterSt==operaterUporanika){
							kliciDomace+=podatek.getInt("trajanje");
						}
						else{
							kliciTuje+=podatek.getInt("trajanje");
						}
					}
					break;
				}				
				
			}
			else{
				// Obravnavamo tujino
				tip=podatek.getString("tip").charAt(0);
				odhodni=podatek.getInt("odhodni_dohodni");
				
				switch (tip) {
				case 's':
					if(odhodni==1){
						smsMmsTujina++;
					}
					break;
				case 'm':
					if(odhodni==1){
						smsMmsTujina++;
					}
					break;
				case 'n':
					netTujina+=podatek.getInt("kolicina");
					break;
				case 'k':
					if(odhodni==1){
						kliciOdhodniTujina+=podatek.getInt("trajanje");
					}
					else{
						kliciDohodniTujina+=podatek.getInt("trajanje");
					}
					break;
				}
			}
			
		}
		
		kliciDomace/=60;
		kliciTuje/=60;
		kliciStac/=60;
		net/=1024;
		
		kliciOdhodniTujina/=60;
		kliciDohodniTujina/=60;
		netTujina/=1024;
		
		int skupnaKol;
		int smsMmsKol;
		int netKol;
		int kliciDomaceKol;
		int kliciTujeStacKol;
		
		Storitev s=new Storitev(storitev);
		RezultatStroskov rs=new RezultatStroskov();
		
		//Dodatne enote
		skupnaKol=s.getZakupljenaKolicinaSkupna();
		smsMmsKol=s.getZakupljenaKolicinaSmsMms();
		netKol=s.getZakupljenaKolicinaNet();
		kliciDomaceKol=s.getZakupljenaKolicinaKliciDomace();
		kliciTujeStacKol=s.getZakupljenaKolicinaKliciTujeStac();
		
		rs=izracunajDodatneStroske(skupnaKol,netKol,'n',s,net);
		cenaEnot+=rs.dodatniStroski;
		skupnaKol=rs.skupnaKolicina;
		
		rs=izracunajDodatneStroske(skupnaKol,smsMmsKol,'m',s,smsMms);
		cenaEnot+=rs.dodatniStroski;
		skupnaKol=rs.skupnaKolicina;
		
		rs=izracunajDodatneStroske(skupnaKol,kliciDomaceKol,'d',s,kliciDomace);
		cenaEnot+=rs.dodatniStroski;
		skupnaKol=rs.skupnaKolicina;
		
		rs=izracunajDodatneStroske(skupnaKol,kliciTujeStacKol,'t',s,kliciTuje);
		cenaEnot+=rs.dodatniStroski;
		skupnaKol=rs.skupnaKolicina;
		kliciTujeStacKol=rs.kolicina;
		
		rs=izracunajDodatneStroske(skupnaKol,kliciTujeStacKol,'s',s,kliciStac);
		cenaEnot+=rs.dodatniStroski;
		
		//Tujina
		cenaTujina+=gostovanje.vrniCenoEnot(drzava, 'o',kliciOdhodniTujina);
		cenaTujina+=gostovanje.vrniCenoEnot(drzava, 'd',kliciDohodniTujina);
		cenaTujina+=gostovanje.vrniCenoEnot(drzava, 's',smsMmsTujina);
		cenaTujina+=gostovanje.vrniCenoEnot(drzava, 'n',netTujina);
		
		double cenaSkupna=mesecnaNarocnina+cenaEnot+cenaTujina;
		
		rezultat.nastaviVrednost("mesecnaNarocnina", df.format(mesecnaNarocnina));
		rezultat.nastaviVrednost("cenaEnot", df.format(cenaEnot));
		rezultat.nastaviVrednost("cenaTujina", df.format(cenaTujina));
		rezultat.nastaviVrednost("cenaSkupna", df.format(cenaSkupna));
		
		baza.zapriPovezavo();
		
		return rezultat;
	}
	
	private static RezultatStroskov izracunajDodatneStroske(int skupnaKol,int kolicina,char tip, Storitev s, int kolicinaEnot){
		int razlika;
		RezultatStroskov rs=new RezultatStroskov();
		
		if(skupnaKol>0){
			
			if(kolicina!=-1){
				
				razlika=skupnaKol-kolicinaEnot;
				
				if(razlika<0){
					rs.dodatniStroski=Math.abs(razlika)*dobiCenoEnote(tip, s);
				}
				else{
					rs.skupnaKolicina=razlika;
				}
				
			}
			else{
				rs.skupnaKolicina=skupnaKol;
			}
		}
		else{
			
			if(kolicina!=-1){
				
				razlika=kolicina-kolicinaEnot;
				
				if(razlika<0){
					rs.dodatniStroski=Math.abs(razlika)*dobiCenoEnote(tip, s);
				}
				else{
					rs.kolicina=razlika;
				}
			}
		}
		
		return rs;
	}
	
	private static double dobiCenoEnote(char tip, Storitev s){
		switch (tip) {
		case 'n':
			return s.getCenaNaEnotoNet();
		case 'm':
			return s.getCenaNaEnotoSmsMms();
		case 'd':
			return s.getCenaNaEnotoKlicDomace();
		case 't':
			return s.getCenaNaEnotoKlicTuje();
		case 's':
			return s.getCenaNaEnotoKlicStac();
		default:
			return 0.0;
		}
	}
	
	public static String izpisEnot(Object enote){
		Integer e=(Integer)enote;
		
		if(e==-1){
			return "∞";
		}
		
		return e+"";
	}

	public static String izpisCene(Object enote){
		if (enote instanceof Integer) {
			return enote+"";
		}
		DecimalFormat df = new DecimalFormat("0.##############");
		return df.format((Double)enote);
	}
	
	public static String izpisiDatum(Date date){		
		return new SimpleDateFormat("dd.MM.yyyy").format(date);
	}
	
	public static boolean ustreznoGeslo(String idUporabnika, String geslo){
		
		Baza baza=new Baza();
		
		String gesloMD5=MD5(geslo);
		
		BasicDBObject uporabnik;
		ObjectId idUpo=new ObjectId(idUporabnika);
		BasicDBObject pogoji=new BasicDBObject("_id", idUpo);
		
		uporabnik=baza.dobiEnega("uporabnik", pogoji, null, null);
		
		baza.zapriPovezavo();
		
		if(gesloMD5.equals(uporabnik.getString("geslo"))){
			return true;
		}
		
		return false;
	}
	
	private static String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
}
