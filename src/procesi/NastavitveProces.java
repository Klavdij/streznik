package procesi;

import java.util.ArrayList;

import org.bson.types.ObjectId;

import orodja.Baza;
import rand.Randomizer;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

public class NastavitveProces {
	
	public static String emsoUporabnika(String uporabnik){
		String emso="";
		
		Baza b=new Baza();
		
		ObjectId idUpo=new ObjectId(uporabnik);
		
		BasicDBList upo=b.dobiPodatke("uporabnik", new BasicDBObject("_id", idUpo), null);
		 
		emso=((BasicDBObject)upo.get(0)).getString("emso");
		
		b.zapriPovezavo();
		
		return emso;
	}
	
	public static void generirajPodatke(String nastavitve){
		
		Randomizer.generiraj(nastavitve);
		
	}
	
	public static boolean shraniStoritev(ArrayList<String[]> list,String id){
		
		Baza b=new Baza();
		
		boolean result=b.shraniStoritev(list,id);
		
		b.zapriPovezavo();
		
		return result;
	}
	
	public static BasicDBList listStoritev(){
		Baza b=new Baza();
		
		BasicDBList list=b.dobiPodatke("storitev", null, null);
		
		b.zapriPovezavo();
		
		return list;
	}
	
	public static boolean izbrisiStoritev(String id){
		Baza b=new Baza();
		boolean result=false;
		
		ObjectId idStoritve=new ObjectId(id);
		BasicDBObject document = new BasicDBObject("_id", idStoritve);

		result=b.zbrisiPodatke("storitev", document);
		
		b.zapriPovezavo();
		
		return result;
	}

}
