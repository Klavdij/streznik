package orodja;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.bson.types.ObjectId;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import entitete.Entiteta;

public class Baza {
	
	private MongoClient mongoKlient;
	private DB db;
	
	private Properties DBprop;

	public Baza() {
		try{
			
			DBprop=new Properties();
			
			// Nalozi nastavitveno datoteko
			DBprop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("database.properties"));
			
			mongoKlient = new MongoClient( DBprop.getProperty("HOST") , Integer.parseInt(DBprop.getProperty("PORT")));
			
			db=mongoKlient.getDB(DBprop.getProperty("DATABASE"));
			
			if(!(DBprop.getProperty("HOST").equals("localhost"))){
				db.authenticate(DBprop.getProperty("USERNAME"), DBprop.getProperty("PASSWORD").toCharArray());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private DBCursor dobiKurzor(String zbirka, BasicDBObject pogoji, BasicDBObject sortiranje){
		DBCollection z=db.getCollection(zbirka);
		
		DBCursor kurzor;
		
		if(pogoji==null){
			kurzor = z.find();
		}
		else{
			kurzor = z.find(pogoji);
		}
		
		if(sortiranje!=null){
			kurzor=kurzor.sort(sortiranje);
		}
		
		return kurzor;
	}
	
	public ObjectId dobiIdObjekta(String zbirka, ObjectId idUporabnika, int leto, int mesec){
		
		BasicDBObject pogoji=new BasicDBObject("id_uporabnika", idUporabnika);
		pogoji.append("leto", leto);
		pogoji.append("mesec", mesec);
		
		return dobiEnega(zbirka, pogoji, null, null).getObjectId("_id");
		
	}
	
	public BasicDBList dobiPodatke(String zbirka, BasicDBObject pogoji, BasicDBObject sortiranje){
		
		DBCursor kurzor=dobiKurzor(zbirka, pogoji, sortiranje);
		
		BasicDBObject objekt;
		BasicDBList dbList=new BasicDBList();

		try {
		   while(kurzor.hasNext()) {
		       objekt= (BasicDBObject) kurzor.next();
		       dbList.add(objekt);
		   }
		} finally {
		   kurzor.close();
		}
		
		return dbList;
	}
	
	public BasicDBObject dobiEnega(String zbirka, BasicDBObject pogoji, BasicDBObject polja, BasicDBObject sortiranje){
		
		DBCollection z=db.getCollection(zbirka);
		
		BasicDBObject objekt=(BasicDBObject) z.findOne(pogoji, polja, sortiranje);
		
		return objekt;
	}
	
	public int dobiStPodatkov(String zbirka, BasicDBObject pogoji){
		DBCursor kurzor=dobiKurzor(zbirka, pogoji, null);
		
		int st=kurzor.size();
		kurzor.close();
		
		return st;
		
	}
	
	public int dobiSestevekPodatkov(String zbirka, BasicDBObject pogoji){
		BasicDBObject objekt;
		DBCursor kuruzor=dobiKurzor(zbirka, pogoji, null);
		
		String tip=pogoji.getString("tip");
		
		int sum=0;
		double sumDou=0.0;
		
		try {
			   while(kuruzor.hasNext()) {
				   objekt= (BasicDBObject) kuruzor.next();
			       if(tip.equals("k")){
			    	   sum+=objekt.getInt("trajanje");
			       }
			       else{
			    	   sumDou+=objekt.getDouble("kolicina");
			       }
			   }
			} finally {
				kuruzor.close();
			}
		
		if(tip.equals("k")){
			sum/=60;
		}
		else{
			sum=(int)Math.round((sumDou/1024.0));
		}
		
		return sum;
	}
	
	public void shraniPodatke(String emso, JsonObject podatki){
		BasicDBObject doc;
		DBCollection z=db.getCollection("podatki");
		
		DBCollection uporabniki=db.getCollection("uporabnik");
		ObjectId idUporabnika=((BasicDBObject)uporabniki.find(new BasicDBObject("emso", emso)).next()).getObjectId("_id");
		
		JsonArray list=(JsonArray)podatki.get("podatki");
		
		for (int i = 0; i < list.size(); i++) {
			
			JsonObject podatek=(JsonObject)list.get(i);
			
			doc = new BasicDBObject();
			
			doc.put("id_uporabnika",idUporabnika);
			doc.put("datum", new Date(podatek.get("datum").getAsLong()));
			doc.put("lokacija_x", podatek.get("lokacija_x").getAsDouble());
			doc.put("lokacija_y", podatek.get("lokacija_y").getAsDouble());
			doc.put("tip", podatek.get("tip").getAsCharacter());
			doc.put("odhodni_dohodni", podatek.get("odhodni_dohodni").getAsInt());
			doc.put("stevilka", podatek.get("stevilka").getAsInt());
			doc.put("trajanje", podatek.get("trajanje").getAsInt());
			doc.put("kolicina", podatek.get("kolicina").getAsInt());
			
			z.insert(doc);
			
		}
	}
	
	public void shraniStandardnePodatke(Entiteta e){
		DBCollection zbirka=db.getCollection(e.zbirka());
		BasicDBObject dokument=e.dobiPodatkovniObjekt();
		
		if(dokument.getObjectId("_id")==null){
			zbirka.insert(dokument);
		}
		else{
			BasicDBObject id=new BasicDBObject("_id", dokument.getObjectId("_id"));
			
			zbirka.update(id, dokument);
		}		
	}
	
	public boolean zbrisiPodatke(String zbirka, BasicDBObject dokument){
		DBCollection z=db.getCollection(zbirka);
		try{
			z.remove(dokument);
		}
		catch(MongoException me){
			return false;
		}
		
		return true;
		
	}
	
	public boolean shraniStoritev(ArrayList<String[]> list,String id){
		BasicDBObject dokument = new BasicDBObject();
		DBCollection z=db.getCollection("storitev");
		
		if(id!=null){
			dokument.put("_id", new ObjectId(id));
		}
		
		for (int i = 0; i < list.size(); i++) {
			dokument.put(list.get(i)[0], list.get(i)[1]);
		}
		
		try{
			if(id!=null){
				z.save(dokument);
			}
			else{
				z.insert(dokument);
			}
		}
		catch(MongoException me){
			return false;
		}
		
		return true;
	}
	
	public BasicDBObject dobiStoritevUporabnika(ObjectId idUpo){
		DBCollection zbirkaUporabnika=db.getCollection("uporabnik");
		
		int velikost;
		BasicDBObject pogoj=new BasicDBObject("_id", idUpo);
		
		BasicDBObject uporabnik=(BasicDBObject)zbirkaUporabnika.findOne(pogoj);
		
		BasicDBList storitve=(BasicDBList)uporabnik.get("storitev");
		
		velikost=storitve.size();
		
		BasicDBObject storitevUporabnika=(BasicDBObject)storitve.get(velikost-1);
		
		ObjectId idStoritveUporanbika=storitevUporabnika.getObjectId("id_storitve");
		
		DBCollection storitevColl=db.getCollection("storitev");
		
		BasicDBObject storitev=(BasicDBObject)storitevColl.findOne(new BasicDBObject("_id", idStoritveUporanbika));
		
		storitev.append("datum_od", storitevUporabnika.get("datum_od"));
		storitev.append("datum_do", storitevUporabnika.get("datum_do"));
		
		return storitev;
		
	}
	
	public void zapriPovezavo(){
		mongoKlient.close();
	}
	
}
