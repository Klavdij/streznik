package orodja.nastavitve;

import java.io.IOException;
import java.util.Properties;

public class Nastavitve {
	
	private Properties prop;

	public Nastavitve() {
		
		prop=new Properties();
		
		try {
			
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("general.properties"));
			
		} catch (IOException e) {
			System.out.println("Napaka pri odpiranju datoteke z nastavitvemi.");
		}
		
	}
	
	public OdstopanjeNastavitve vrniNastavitveOdstopanja(){
		OdstopanjeNastavitve nastavitve=new OdstopanjeNastavitve();
		
		try{
			nastavitve.kliciDomace=Integer.parseInt(prop.getProperty("ODSTOPANJE_KLICI_DOMACE"));
			nastavitve.kliciStac=Integer.parseInt(prop.getProperty("ODSTOPANJE_KLICI_TUJE"));
			nastavitve.kliciTuje=Integer.parseInt(prop.getProperty("ODSTOPANJE_KLICI_STAC"));
			nastavitve.smsMms=Integer.parseInt(prop.getProperty("ODSTOPANJE_SMS_MMS"));
			nastavitve.net=Integer.parseInt(prop.getProperty("ODSTOPANJE_NET"));
			
		} catch(Exception e){
			System.out.println("Napaka pri parsanju nastavitev.");
		}	
		
		return nastavitve;
	}
	
	public TujinaOdstopanjeNastavitve vrniNastavitveOdstopanjaTujine(){
		TujinaOdstopanjeNastavitve nastavitve=new TujinaOdstopanjeNastavitve();
		
		try{
			nastavitve.stOdstopanj=Integer.parseInt(prop.getProperty("TUJINA_STEVILO_ODSTOPANJ"));
			nastavitve.stDrzav=Integer.parseInt(prop.getProperty("TUJINA_STEVILO_DRZAV"));
			nastavitve.cena=Double.parseDouble(prop.getProperty("TUJINA_CENA"));
			
		} catch(Exception e){
			System.out.println("Napaka pri parsanju nastavitev.");
		}	
		
		return nastavitve;
	}
	
	public GostovanjeNastavitve vrniNastavitveGostovanja(){
		GostovanjeNastavitve nastavitve=new GostovanjeNastavitve();
		
		try{
			nastavitve.obracunskiIntervalKlic=Integer.parseInt(prop.getProperty("TUJINA_OBRACUNSKI_INTERVAL_KLIC"));
			nastavitve.obracunskiIntervalEnota=Integer.parseInt(prop.getProperty("TUJINA_OBRACUNSKI_INTERVAL_ENOTA"));
			nastavitve.obracunskiIntervalNet=Integer.parseInt(prop.getProperty("TUJINA_OBRACUNSKI_INTERVAL_NET"));
			nastavitve.cenaKliciOdhodni=Double.parseDouble(prop.getProperty("TUJINA_CENA_KLICI_ODHODNI"));
			nastavitve.cenaKliciDohodni=Double.parseDouble(prop.getProperty("TUJINA_CENA_KLICI_DOHODNI"));
			nastavitve.cenaSmsMms=Double.parseDouble(prop.getProperty("TUJINA_CENA_SMS_MMS"));
			nastavitve.cenaNet=Double.parseDouble(prop.getProperty("TUJINA_CENA_NET"));
			
		} catch(Exception e){
			System.out.println("Napaka pri parsanju nastavitev.");
		}	
		
		return nastavitve;
	}
	
	public MesecnaStatistikaNastavitve vrniNastavitveMesecneStatistike(){
		MesecnaStatistikaNastavitve nastavitve=new MesecnaStatistikaNastavitve();
		
		try{
			nastavitve.dniTestneMnozice=Integer.parseInt(prop.getProperty("MS_ST_DNI_MESECNE_TESTNE_MNOZICE"));
			nastavitve.dovoljenProcentOdstopanja=Double.parseDouble(prop.getProperty("MS_DOVOLJEN_PROCENT_ODSTOPANJA"));
			nastavitve.minimalnoSteviloEnot=Integer.parseInt(prop.getProperty("MS_MINIMALNO_ST_ENOT"));
			
		} catch(Exception e){
			System.out.println("Napaka pri parsanju nastavitev.");
		}	
		
		return nastavitve;
	}
	
	public ObracunskiIntervalNastavitve vrniNastavitveObracunskegaIntervala(){
		ObracunskiIntervalNastavitve nastavitve=new ObracunskiIntervalNastavitve();
		
		try{
			String[] o;
			nastavitve.steviloKlicev=Integer.parseInt(prop.getProperty("OI_STEVILO_KLICEV_ZA_OBDELAVO"));
			
			String ois=prop.getProperty("OI_OBRACUNSKI_INTERVALI");
			String [] oia=ois.split(",");
			
			nastavitve.imenaObracunskihIntervalov=new String[oia.length];
			nastavitve.obracunskiIntervali=new int [oia.length][2];
			
			for (int i = 0; i < oia.length; i++) {
				nastavitve.imenaObracunskihIntervalov[i]=oia[i];
				o=oia[i].split("/");
				nastavitve.obracunskiIntervali[i][0]=Integer.parseInt(o[0]);
				nastavitve.obracunskiIntervali[i][1]=Integer.parseInt(o[1]);
			}
			
			
		} catch(Exception e){
			System.out.println("Napaka pri parsanju nastavitev.");
		}	
		
		return nastavitve;
	}

}
