package orodja;

public class Klasifikator {
	
	private int povprOdstopanjeMinus;
	private int povprecje;
	private int povprOdstopanjePlus;
	
	private int min;
	private int max;
	
	private int [] razporeditevEnot;
	private int steviloEnot;
	
	
	public Klasifikator() {
		razporeditevEnot=new int[3];
		razporeditevEnot[0]=0;
		razporeditevEnot[1]=0;
		razporeditevEnot[2]=0;
		steviloEnot=0;
	}


	public int getPovprOdstopanjeMinus() {
		return povprOdstopanjeMinus;
	}


	public void setPovprOdstopanjeMinus(int povprOdstopanjeMinus) {
		this.povprOdstopanjeMinus = povprOdstopanjeMinus;
	}


	public int getPovprecje() {
		return povprecje;
	}


	public void setPovprecje(int povprecje) {
		this.povprecje = povprecje;
	}


	public int getPovprOdstopanjePlus() {
		return povprOdstopanjePlus;
	}


	public void setPovprOdstopanjePlus(int povprOdstopanjePlus) {
		this.povprOdstopanjePlus = povprOdstopanjePlus;
	}


	public int getMin() {
		return min;
	}


	public void setMin(int min) {
		this.min = min;
	}


	public int getMax() {
		return max;
	}


	public void setMax(int max) {
		this.max = max;
	}

	public int getMidMin() {
		return povprecje-povprOdstopanjeMinus;
	}
	
	public int getMidMax() {
		return povprecje+povprOdstopanjePlus;
	}

	public double getProcentMin() {
		if(razporeditevEnot[0]>0){
			return 1.0/steviloEnot*razporeditevEnot[0];
		}
		return 0;
	}
	

	public double getProcentMid() {
		if(razporeditevEnot[1]>0){
			return 1.0/steviloEnot*razporeditevEnot[1];
		}
		return 0;
	}


	public double getProcentMax() {
		if(razporeditevEnot[2]>0){
			return 1.0/steviloEnot*razporeditevEnot[2];
		}
		return 0;
	}


	public double[] procenti() {
		double[] p=new double[3];
		p[0]=getProcentMin();
		p[1]=getProcentMid();
		p[2]=getProcentMax();
		
		return p;
	}
	
	
	public int[][] razporeditevEnot(){
		int [][] re=new int [3][2];
		
		re[0][0]=min;
		re[0][1]=getMidMin();
		
		re[1][0]=getMidMin()+1;
		re[1][1]=getMidMax()-1;
		
		re[2][0]=getMidMax();
		re[2][1]=max;
		
		return re;
	}
	

	public void dodajEnoto(int enota){
		int midMin=povprecje-povprOdstopanjeMinus;
		int midMax=povprecje+povprOdstopanjePlus;
		
		if(enota>=min && enota<=midMin){
			razporeditevEnot[0]++;
		}
		else if (enota>midMin && enota<midMax){
			razporeditevEnot[1]++;
		}
		else{
			razporeditevEnot[2]++;
		}
		
		steviloEnot++;
	}

	@Override
	public String toString() {
		return min+" - "+povprOdstopanjeMinus+" "+povprecje+" "+povprOdstopanjePlus+" - "+max+"\n"+
				getProcentMin()+" -  "+getProcentMid()+"  - "+getProcentMax();
		
	}
	
}
