package orodja;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class Lokacija {
	
	
	private HashMap<String, String> lokacije;
	
	public Lokacija() {
		lokacije=new HashMap<String, String>();
	}

	public String getCountry(double x, double y){
		String country="";
		
		String keyLocation=x+","+y;
		
		if(lokacije.containsKey(keyLocation)){
			country=lokacije.get(keyLocation);
		}
		else{
			
			try {
			    URL myURL = new URL("http://maps.googleapis.com/maps/api/geocode/json?latlng="+keyLocation+"&sensor=false");
			    URLConnection myURLConnection = myURL.openConnection();
			    myURLConnection.connect();
			    
			    InputStream is=myURLConnection.getInputStream();
			    
			    String line;
			    StringBuilder sb = new StringBuilder();
	
			    BufferedReader br = new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
				
				country = sb.toString();
				
				JsonParser parser = new JsonParser();
				JsonObject podatki=(JsonObject)parser.parse(country);
				
				if(podatki.get("status").getAsString().equals("OK")){
					JsonArray results=podatki.getAsJsonArray("results");
					
					JsonObject tmp;
					String address;
					
					for (int i = 0; i < results.size(); i++) {
						tmp=results.get(i).getAsJsonObject();
						address=tmp.get("formatted_address").getAsString();
						if(address.indexOf(',')==-1){
							country=address;
							lokacije.put(keyLocation, country);
							break;
						}
					}
				}
				
				
				br.close();
			    is.close();
			} 
			catch (Exception e) { 
			    System.out.println(e.getStackTrace());
			}
		}
		
		return country;
	}

}
