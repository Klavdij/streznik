package orodja;

import java.util.HashMap;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import orodja.nastavitve.GostovanjeNastavitve;
import orodja.nastavitve.Nastavitve;

public class Gostovanje {
	
	private GostovanjeNastavitve standardneEnote;
	private HashMap<String, GostovanjeNastavitve> gostovanjePodatki;
	
	public Gostovanje() {
		
		Nastavitve nastavitve=new Nastavitve();
		standardneEnote=nastavitve.vrniNastavitveGostovanja();
		
		Baza baza=new Baza();
		
		BasicDBObject lgo;
		GostovanjeNastavitve gn;
		BasicDBList listaGostovanj=baza.dobiPodatke("gostovanje", null, null);
		
		gostovanjePodatki=new HashMap<String, GostovanjeNastavitve>();
		
		for (int i = 0; i < listaGostovanj.size(); i++) {
			lgo= (BasicDBObject)listaGostovanj.get(i);
			
			gn=new GostovanjeNastavitve();
			gn.obracunskiIntervalKlic=lgo.getInt("obracunski_interval_klic");
			gn.obracunskiIntervalEnota=lgo.getInt("obracunski_interval_enota");
			gn.obracunskiIntervalNet=lgo.getInt("obracunski_interval_net");
			gn.cenaKliciOdhodni=lgo.getDouble("cena_na_enoto_klic");
			gn.cenaKliciDohodni=lgo.getDouble("cena_na_enoto_klic_dohodni");
			gn.cenaSmsMms=lgo.getDouble("cena_na_enoto_sms_mms");
			gn.cenaNet=lgo.getDouble("cena_na_enoto_net");
			
			gostovanjePodatki.put(lgo.getString("drzava"), gn);
		}
		
		baza.zapriPovezavo();
		
	}
	
	public double vrniCenoEnot(String drzava, char tip, int enot){
		
		GostovanjeNastavitve gn;
		
		gn=gostovanjePodatki.get(drzava);
		
		if(gn==null){
			gn=standardneEnote;
		}
		
		return vrniCenoEnote(gn, tip)*enot;
	}
	
	private double vrniCenoEnote(GostovanjeNastavitve gn,char tip){
		
		switch (tip) {
		case 'o':
			return gn.cenaKliciOdhodni;
		case 'd':
			return gn.cenaKliciDohodni;
		case 's':
			return gn.cenaSmsMms;
		case 'n':
			return gn.cenaNet;
		default:
			return 0.0;
		}
	}
	
}
