package orodja;

import java.util.ArrayList;

public class Operater {
	
	public static final int MOBITEL=0;
	public static final int IZIMOBIL=1;
	public static final int DEBITEL=2;
	public static final int SIMOBIL=3;
	public static final int BOB=4;
	public static final int TUSMOBIL=5;
	public static final int T2=6;
	
	public static final int STACIONARNA=7;
	public static final int POSEBNA=8;
	public static final int VOIP=9;
	
	public static final String[] imenaOperaterjev={"Mobitel","Izimobil","Debitel","Simobil","Bob","Tusmobil","T2","Stacionarna","Posebna","VoIP"};

	public static final int[][] stevilke={
		{31,41},//Mobitel
		{51},//Izimobil
		{71},//Debitel
		{30,40},//Simobil
		{68},//Bob
		{70},//Tusmobil
		{64},//T2
		{1,2,3,4,5,7},//Stacionarna
		{80,89,90},//Posebna
		{8200,8201,8202,8205,8209}//VoIP
	};
	
	public static int vrniOperaterja(int stevilka){
		for (int i = 0; i < stevilke.length; i++) {
			for (int j = 0; j < stevilke[i].length; j++) {
				if(stevilka==stevilke[i][j]){
					return i;
				}
			}
		}
		
		return -1;
	}
	
	public static int[] vrniStevilke(int operater){
		return stevilke[operater];
	}
	
	public static int[]vrniStevilkeOstalih(int operater){
		ArrayList<Integer> list=new ArrayList<Integer>();
		int[]st;
		for (int i = 0; i < stevilke.length; i++) {
			if(i!=operater){
				for (int j = 0; j < stevilke[i].length; j++) {
					list.add(stevilke[i][j]);
				}
			}
		}
		st=new int[list.size()];
		for (int i = 0; i < st.length; i++) {
			st[i]=list.get(i).intValue();
		}
		return st;
	}
	
	public static String vrniImeOperaterja(int operater){
		return imenaOperaterjev[operater];
	}
	
	public static int vrniOperaterja(String imeOperaterja){
		for (int i = 0; i < imenaOperaterjev.length; i++) {
			if(imenaOperaterjev[i].equals(imeOperaterja)){
				return i;
			}
		}
		return -1;
	}
}
